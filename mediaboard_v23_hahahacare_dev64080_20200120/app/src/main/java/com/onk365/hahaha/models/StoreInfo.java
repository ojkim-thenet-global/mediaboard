package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class StoreInfo {

    @SerializedName("store_id")
    private String store_id;
    @SerializedName("store_seq")
    private int store_seq;
    @SerializedName("store_no")
    private String store_no;
    @SerializedName("store_name")
    private String store_name;
    @SerializedName("address_pref")
    private String address_pref;
    @SerializedName("address_county")
    private String address_county;
    @SerializedName("x")
    private int x;
    @SerializedName("y")
    private int y;
    @SerializedName("no")
    private String no;

    public String getStoreId() {
        return store_id;
    }
    public void setStoreId(String store_id) {   this.store_id = store_id;}

    public int getStoreSeq() {
        return store_seq;
    }
    public void setStoreSeq(int store_seq) {   this.store_seq = store_seq;}

    public String getStoreNo() {
        return store_no;
    }
    public void setStoreNo(String store_no) {
        this.store_no = store_no;
    }

    public String getStoreName() {
        return store_name;
    }
    public void setStoreName(String store_name) {
        this.store_name = store_name;
    }
    public String getAddressPref() {
        return address_pref;
    }
    public void setAddressPref(String address_pref) {
        this.address_pref = address_pref;
    }
    public String getAddressCounty() {
        return address_county;
    }
    public void setAddressCounty(String address_county) {this.address_county = address_county;}
    public int getX() {
        return x;
    }
    public void setX(int x) {   this.x = x;}
    public int getY() {
        return y;
    }
    public void setY(int y) {   this.y = y;}
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }

}