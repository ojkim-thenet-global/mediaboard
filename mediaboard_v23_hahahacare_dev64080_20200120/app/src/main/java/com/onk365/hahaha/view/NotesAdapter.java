package com.onk365.hahaha.view;

/**
 * Created by ravi on 20/02/18.
 */

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.onk365.hahaha.R;
import com.onk365.hahaha.models.Adver;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.MyViewHolder> {

    private Context context;
    private List<Adver> notesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
       // public TextView note;
        //public TextView dot;
        //public TextView timestamp;
        public ImageView thumbimage;

        public MyViewHolder(View view) {
            super(view);
            thumbimage = view.findViewById(R.id.thumbimage);
           // dot = view.findViewById(R.id.dot);
            //timestamp = view.findViewById(R.id.timestamp);
        }
    }


    public NotesAdapter(Context context, List<Adver> notesList) {
        this.context = context;
        this.notesList = notesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Adver adver = notesList.get(position);

        //holder.note.setText(adver.getM0Type());

        // Displaying dot from HTML character code
       // holder.dot.setText(Html.fromHtml("&#8226;"));

        // Formatting and displaying timestamp
        //holder.timestamp.setText(formatDate(adver.getM0Src()));

        //ImageView imageView= (ImageView)view.findViewById(R.id.image_view);
        String thumb = adver.getThumbSrc().replace("/", "_");
        //Log.d("11111111111111111", thumb);
        File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + thumb);
        Glide.with(context).load(c).into(holder.thumbimage);
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }

    /**
     * Formatting timestamp to `MMM d` format
     * Input: 2018-02-21 00:15:42
     * Output: Feb 21
     */
    private String formatDate(String dateStr) {
        try {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = fmt.parse(dateStr);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM d");
            return fmtOut.format(date);
        } catch (ParseException e) {

        }

        return "";
    }
}
