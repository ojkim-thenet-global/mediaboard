package com.onk365.hahaha.view;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.onk365.hahaha.R;
import com.onk365.hahaha.models.Adver;
import com.onk365.hahaha.models.Weathers;
import com.onk365.hahaha.models.WeathersResponse;

import java.io.File;
import java.util.List;

/**
 * Created by Junho on 2017-03-02.
 */
public class ViewPagerAdapter extends PagerAdapter {
//public class ViewPagerAdapter extends PagerAdapter implements Html.ImageGetter {


    //ArrayList arrayList;
    private List<Adver> adversList;
    private List<WeathersResponse> weahersList;
    LayoutInflater inflater;
    private Context context;
    private String master_upper = "";

    private MediaController mediaController;



    //public ViewPagerAdapter(LayoutInflater inflater , List<Adver> adversList, List<WeathersResponse> weahersList, Context context) {
    public ViewPagerAdapter(LayoutInflater inflater , List<Adver> adversList, List<WeathersResponse> weahersList, String master_upper, Context context) {

        this.inflater=inflater;
        this.adversList = adversList;
        this.weahersList = weahersList;
        this.context = context;
        this.master_upper = master_upper;
    }
    @Override
    public int getCount() {
        //return arrayList.size();
        return adversList.size();
    }



    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Adver adver = adversList.get(position);
        String m0type = adver.getM0Type();
        String m0src ;

        View  view = null;
        ImageView image_bnr = null;

        //날씨 슬라이드
        if(adver.getCategoryNo() < 10) {

            view= inflater.inflate(R.layout.adapter_weather2, null);

            TextView text1= (TextView)view.findViewById(R.id.text1);
            TextView text2= (TextView)view.findViewById(R.id.text2);
            TextView text3= (TextView)view.findViewById(R.id.text3);
            TextView text4= (TextView)view.findViewById(R.id.text4);
            TextView text5= (TextView)view.findViewById(R.id.text5);
            TextView text6= (TextView)view.findViewById(R.id.text6);
            TextView text7= (TextView)view.findViewById(R.id.text7);
            TextView text8= (TextView)view.findViewById(R.id.text8);
            TextView text9= (TextView)view.findViewById(R.id.text9);
            m0src = adver.getM0Src().replace("/", "_");

            ImageView imageView= (ImageView)view.findViewById(R.id.board);
            File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + m0src);
            Glide.with(imageView.getContext()).load(c).into(imageView);

            //띠광고
            ImageView upper= (ImageView)view.findViewById(R.id.upper);
            c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + master_upper);
            Glide.with(upper.getContext()).load(c).into(upper);


            String label = "미측정";
            Weathers.Cold cold = weahersList.get(0).getCold();

            //감기지수 9, 10, 11, 12, 1, 2, 3, 4월
            if(!TextUtils.isEmpty(cold.getToday()) && !cold.getToday().equals("-") && !cold.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(cold.getToday()), "COLD");
                text1.setText(label);
            } else {
                //label = makeLifeLabel(0, "COLD");
                text1.setText(label);
            }
            //피부질환
            label = "미측정";
            Weathers.Skin skin = weahersList.get(0).getSkin();
            if(!TextUtils.isEmpty(skin.getToday()) && !skin.getToday().equals("-") && !skin.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(skin.getToday()), "SKIN_DISEASE");
                text2.setText(label);
            } else {
                //label = makeLifeLabel(0, "SKIN_DISEASE");
                text2.setText(label);
            }
            //호흡기질환지수
            label = "미측정";
            Weathers.Respiratory respiratory = weahersList.get(0).getRespiratory();
            if(!TextUtils.isEmpty(respiratory.getToday()) && !respiratory.getToday().equals("-") && !respiratory.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(respiratory.getToday()), "RESPIRATORY_DISEASE");
                text3.setText(label);
            } else {
                //label = makeLifeLabel(0, "RESPIRATORY_DISEASE");
                text3.setText(label);
            }
            //뇌졸중 가능지수
            label = "미측정";
            Weathers.Stroke stroke = weahersList.get(0).getStroke();
            if(!TextUtils.isEmpty(stroke.getToday()) && !stroke.getToday().equals("-") && !stroke.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(stroke.getToday()), "STROKE");
                text4.setText(label);
            } else {
                text4.setText(label);
            }
            //자외선지수
            label = "미측정";
            Weathers.Uv uv = weahersList.get(0).getUv();
            if(!TextUtils.isEmpty(uv.getToday()) && !uv.getToday().equals("-") && !uv.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(uv.getToday()), "UV_RAY");
                text5.setText(label);
            } else {
                text5.setText(label);
            }
            //체감온도 12월, 1 , 2, 3월
            label = "미측정";
            Weathers.Wind wind = weahersList.get(0).getWind();
            if(!TextUtils.isEmpty(wind.getToday()) && !wind.getToday().equals("-") && !wind.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(wind.getToday()), "WIND_CHILL");
                text6.setText(label);
            } else {
                //label = makeLifeLabel(0, "WIND_CHILL");
                text6.setText(label);
            }
            //식중독
            label = "미측정";
            Weathers.Food food = weahersList.get(0).getFood();
            if(!TextUtils.isEmpty(food.getToday()) && !food.getToday().equals("-") && !food.getToday().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(food.getToday()), "FOOD_POISONING");
                text7.setText(label);
            } else {
                text7.setText(label);
            }
            //동파
            label = "미측정";
            Weathers.Frozen frozen = weahersList.get(0).getFrozen();
            if(!TextUtils.isEmpty(frozen.getH3()) && !frozen.getH3().equals("-") && !frozen.getH3().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(frozen.getH3()), "FROZEN");
                text8.setText(label);
            } else {
                //label = makeLifeLabel(0, "FROZEN");
                text8.setText(label);
            }
            //불쾌 6, 7, 8, 9월
            label = "미측정";
            Weathers.Discomfort discomfort = weahersList.get(0).getDiscomfort();
            if(!TextUtils.isEmpty(discomfort.getH3()) && !discomfort.getH3().equals("-") && !discomfort.getH3().equals("999")) {
                label = makeLifeLabel(Integer.parseInt(discomfort.getH3()), "DISCOMFORT");
                text9.setText(label);
            } else {
                //label = makeLifeLabel(0, "DISCOMFORT");
                text9.setText(label);
            }
        } else {


            if(m0type.equals("10")) {//이미지


                view= inflater.inflate(R.layout.adapter, null);


                //LinearLayout adapter_layout = (LinearLayout) view.findViewById(R.id.adapter_layout);
                //adapter_layout.setDescendantFocusability( ViewGroup.FOCUS_BEFORE_DESCENDANTS );


                m0src = adver.getM0Src().replace("/", "_");
                ImageView imageView= (ImageView)view.findViewById(R.id.image_view);
                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + m0src);
                Glide.with(imageView.getContext()).load(c).into(imageView);


                //띠광고
                ImageView upper= (ImageView)view.findViewById(R.id.upper);
                //c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_footer_upper.jpg");
                c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + master_upper);
                Glide.with(upper.getContext()).load(c).into(upper);



            } else if ( (m0type.equals("20")) || (m0type.equals("21")) || (m0type.equals("22")) || (m0type.equals("23")) || (m0type.equals("24")) || (m0type.equals("25")) || (m0type.equals("26")) || (m0type.equals("27")) ) {
                // 동영상

                String m1src = adver.getM1Src().replace("/", "_");


                /*
                  video layout
                  1. video + image banner + rolling banner
                  2. video + image banner
                  3. 1:1 video + image banner
                  4. full video
                 */
                if(m0type.equals("20") || m0type.equals("22") || m0type.equals("24") || m0type.equals("26")) {

                    m0src = adver.getM0Src().replace("/", "_");

                    // 태그 번호
                    String layout_num = "";


                    if(m0type.equals("20")){
                        layout_num = "20";
                        view = inflater.inflate(R.layout.adapter_video, null);
                        //view = inflater.inflate(R.layout.adapter_video, null);

                    }else if(m0type.equals("22")){
                        layout_num = "22";
                        view = inflater.inflate(R.layout.adapter_video_layout_4, null);

                    }else if(m0type.equals("24")){
                        layout_num = "24";
                        view = inflater.inflate(R.layout.adapter_video_layout_5, null);

                    }else if(m0type.equals("26")){
                        layout_num = "26";
                        view = inflater.inflate(R.layout.adapter_video_layout_6, null);
                    }


                    VideoView videoView = (VideoView) view.findViewById(R.id.video_view);
                    LinearLayout video_layout = (LinearLayout) view.findViewById(R.id.video_layout);
                    String tag = layout_num + position;
                    video_layout.setTag(tag);


                    // SD카드 경로 HAHAHA 폴더에서 가져오기
                    String video = Environment.getExternalStorageDirectory() + "/hahaha/" + m0src;
                    videoView.setVideoURI(Uri.parse(video));
                    //videoView.setVideoURI(Uri.parse("http://onk.qwave.co.kr:64080/crm/crm_s3_dev/mbContents/common/30/11/1/mov_0.mp4"));



                    // 전체 동영상 제외
                    if( !m0type.equals("26") ){
                        // image banner
                        ImageView imageView= (ImageView)view.findViewById(R.id.image_view);
                        File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + m1src);
                        Glide.with(imageView.getContext()).load(c).into(imageView);
                        //setGlideView(imageView.getContext(), c, imageView);
                    }


                    // rolling banner
                    if(m0type.equals("20")){
                        String bnr1 ="";String bnr2 ="";String bnr3 ="";String bnr4 ="";
                        //콘텐츠가 있는지 체크
                        if(!TextUtils.isEmpty(adver.getM2Src())) {
                            bnr2 = adver.getM2Src().replace("/", "_");
                        }


                        if(!bnr2.equals("")) {
                            image_bnr= (ImageView)view.findViewById(R.id.image_bnr);
                            File d = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + bnr2);
                            Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                        }
                    }


                }else if(m0type.equals("21") || m0type.equals("23") || m0type.equals("25") || m0type.equals("27")) {
                    /*
                      video layout
                      1. video + image banner + rolling banner
                      2. video + image banner
                      3. 1:1 video + image banner
                      4. full video
                     */


                    m0src = adver.getM0Src();
                    //m0src = "https://www.youtube.com/embed/AsXxuIdpkWM";
                    //m0src  = "http://onk.qwave.co.kr:64080/crm/crm_s3_dev/mbContents/common/30/11/1/mov_0.mp4";


                    // 태그 번호
                    String layout_num = "";


                    // 유튜브인지 아닌지 판단
                    int youtube_flag = m0src.indexOf("youtube");

                    if(youtube_flag > -1) {

                        if(m0type.equals("21")){
                            layout_num = "21";
                            view = inflater.inflate(R.layout.adapter_video_youtube, null);
                            //view = inflater.inflate(R.layout.adapter_video, null);

                        }else if(m0type.equals("23")){
                            layout_num = "23";
                            view = inflater.inflate(R.layout.adapter_video_layout_4_youtube, null);

                        }else if(m0type.equals("25")){
                            layout_num = "25";
                            view = inflater.inflate(R.layout.adapter_video_layout_5_youtube, null);

                        }else if(m0type.equals("27")){
                            layout_num = "27";
                            view = inflater.inflate(R.layout.adapter_video_layout_6_youtube, null);
                        }



                        final WebView webView = (WebView) view.findViewById(R.id.youtube_view);
                        LinearLayout youtube_layout = (LinearLayout) view.findViewById(R.id.youtube_layout);

                        String tag = layout_num + position;
                        youtube_layout.setTag(tag);
                        //youtube_layout.setTag("21");


                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                        webView.requestFocusFromTouch();
                        webView.setWebChromeClient(new WebChromeClient());


                        /*
                        // 하드웨어 가속
                        if(Build.VERSION.SDK_INT >= 19){
                            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                        }else{
                            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        }*/


                        // 캐시 비활성화
                        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

                        webView.loadUrl(m0src);
                        //webView.loadDataWithBaseURL(null, m0src, "text/html", "UTF-8", null);

                    }else{


                        if(m0type.equals("21")){
                            layout_num = "20";
                            view = inflater.inflate(R.layout.adapter_video, null);
                            //view = inflater.inflate(R.layout.adapter_video, null);

                        }else if(m0type.equals("23")){
                            layout_num = "22";
                            view = inflater.inflate(R.layout.adapter_video_layout_4, null);

                        }else if(m0type.equals("25")){
                            layout_num = "24";
                            view = inflater.inflate(R.layout.adapter_video_layout_5, null);

                        }else if(m0type.equals("27")){
                            layout_num = "26";
                            view = inflater.inflate(R.layout.adapter_video_layout_6, null);
                        }


                        VideoView videoView = (VideoView) view.findViewById(R.id.video_view);
                        LinearLayout video_layout = (LinearLayout) view.findViewById(R.id.video_layout);
                        String tag = layout_num + position;
                        video_layout.setTag(tag);
                        videoView.setVideoURI(Uri.parse(m0src));

                    } // link end


                    // 전체 유튜브 제외
                    if( !m0type.equals("27") ){
                        // image banner
                        ImageView imageView= (ImageView)view.findViewById(R.id.image_view);
                        File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + m1src);
                        Glide.with(imageView.getContext()).load(c).into(imageView);
                    }


                    // rolling banner
                    if( m0type.equals("21") ){

                        String bnr1 ="";String bnr2 ="";String bnr3 ="";String bnr4 ="";
                        //콘텐츠가 있는지 체크
                        if(!TextUtils.isEmpty(adver.getM2Src())) {
                            bnr2 = adver.getM2Src().replace("/", "_");
                        }

                        if(!bnr2.equals("")) {
                            image_bnr= (ImageView)view.findViewById(R.id.image_bnr);
                            File d = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + bnr2);
                            Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                        }
                    }

                } // youtube, link layout end



            } else if (m0type.equals("30")) {

                view= inflater.inflate(R.layout.adapter_editor, null);

                //m0src = adver.getThumbSrc().replace("/", "_");
                m0src = adver.getM0Src().replace("/", "_");

                if( m0src.indexOf("editor_mb") > -1 ) {

                    ImageView imageView= (ImageView)view.findViewById(R.id.editor_image_view);
                    File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + m0src);
                    Glide.with(imageView.getContext()).load(c).into(imageView);

                }else {

                    view = inflater.inflate(R.layout.adapter_text3, null);
                    WebView webView= (WebView)view.findViewById(R.id.edit_webview);

                    // 자바스크립트 허용
                    webView.getSettings().setJavaScriptEnabled(true);
                    webView.setWebViewClient(new WebViewClient());
                    webView.setWebChromeClient(new WebChromeClient());
                    // 스크롤바 없애기
                    webView.setHorizontalScrollBarEnabled(false);
                    webView.setVerticalScrollBarEnabled(false);
                    // wide viewport를 사용하도록 설정
                    webView.getSettings().setUseWideViewPort(true);
                    webView.getSettings().setLoadWithOverviewMode(true);
                    //webView.getSettings().setDefaultFontSize(36);
                    //webView.getSettings().setMinimumFontSize(14);

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        webView.getSettings().setTextZoom(100);
                    }

                    String data = adver.getM0Src();

                    String sd_card = Environment.getExternalStorageDirectory() + "/hahaha/";

                    if(!TextUtils.isEmpty(adver.getBGType()) && !TextUtils.isEmpty(adver.getBGSrc())) {

                        String bg_type = adver.getBGType();
                        String bg_src = adver.getBGSrc();

                        if (bg_type.equals("90")) {

                            //data = "<html><body style=\"background-color: #591515; \"> "+data+" </body></html>";
                            data = "<html><body style=\"background-color:" + bg_src + "\"> " + data + " </body></html>";

                        } else if (bg_type.equals("10")) {

                            bg_src = bg_src.replace("/", "_");


                            data = " <!doctype html><html>" +
                                    "<head>" +
                                    "<meta charset=\"utf-8\"/>" +
                                    "<meta name=\"viewport\" content=\"width=device-width, shrink-to-fit=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi\" />" +

                                    "<style type=\"text/css\">" +
                                    "html { -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }" +
                                    "#layout { position:relative; left:0; top:0; width:100%; }" +
                                    "#background { position:absolute; top:0; left:0; width:100%; background-size:100% auto; background-repeat:no-repeat; }" +
                                    "#data_layout { position:absolute; top:0; left:0; width:100%; word-break: break-all; }" +
                                    "</style>" +

                                    "</head>" +
                                    "<body>" +
                                    "<div id=\"layout\">" +
                                    "<img id=\"background\" src=\"file://" + sd_card + bg_src +"\">" +              // 배경
                                    "<div id=\"data_layout\">" + data + "</div>" +                                    // 데이터
                                    "</div>" +
                                    "</body>" +
                                    "</html>";
                        }
                    }

                    webView.loadDataWithBaseURL(null, data, "text/html", "UTF-8", "about:blank");
                }

            } else{
                // v23 버전에서 동영상 타입 22, 23, 24, 25, 26, 27 추가
                // 추가된 타입을 처리하는 소스가 없어서 에러발생 -> 예외처리 추가
                view= inflater.inflate(R.layout.adapter_exception_layout, null);

                RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.exception_layout);
                String tag = "10000" + position;
                relativeLayout.setTag(tag);
            }

        }


        //((ViewPager) container).addView((View)view,0);
        container.addView(view);

        return view;
    }






    class CustomWeb extends WebChromeClient {
        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            if(view != null) {
                callback.onCustomViewHidden();
                return;
            }

            view = inflater.inflate(R.layout.adapter_video_youtube, null);

            super.onShowCustomView(view, callback);
        }
    }








    // 생활지수, 보건지수를 표시용 레이블로 변환
    private String makeLifeLabel(int val, String table) {

        String label = "-";
        if(table.equals("UV_RAY")) {
            if (2 >= val ) label  = "낮음";
            if (2 < val && val <= 5) label  = "보통";
            if (5 < val && val <= 7) label  = "높음";
            if (7 < val && val <= 10) label  = "매우높음";
            if (10 < val && val <= 99) label  = "위험";
        }
        if(table.equals("WIND_CHILL")) {
            if (-45 >= val ) label  = "위험";
            if (-45 < val && val <= -25) label  = "경고";
            if (-25< val && val <= -10) label  = "주의";
            if (-10 < val && val <= 99) label  = "관심";
        }
        if(table.equals("FOOD_POISONING")) {
            if (34 >= val ) label  = "관심";
            if (34 < val && val <= 69) label  = "주의";
            if (69< val && val <= 94) label  = "경고";
            if (99 < val && val <= 999) label  = "위험";
        }
        if(table.equals("FROZEN")) {
            if (25 >= val ) label  = "낮음";
            if (25 < val && val <= 50) label  = "보통";
            if (50< val && val <= 75) label  = "높음";
            if (75 < val && val <= 100) label  = "매우높음";
        }
        if(table.equals("DISCOMFORT")) {
            // 불쾌지수 null일 때 '-' 나오게 수정 2019-01-04
            if (0 == val) label ="-";
            if (67 >= val && 0 < val) label  = "낮음";
//            if (67 >= val ) label  = "낮음";
            if (67 < val && val <= 74) label  = "보통";
            if (74< val && val <= 79) label  = "높음";
            if (79 < val && val <= 999) label  = "매우높음";
        }
        if(table.equals("COLD") || table.equals("SKIN_DISEASE") || table.equals("RESPIRATORY_DISEASE") || table.equals("STROKE") ) {
            if (val == 0 ) label  = "낮음";
            if (val == 1 ) label  = "보통";
            if (val == 2 ) label  = "높음";
            if (val == 3 ) label  = "매우높음";
        }

        return label;
        /*
        var UV_RAY = {
                    value : [2, 5, 7, 10, 99],
            label : ["낮음", "보통", "높음", "매우높음", "위험"]
           }
            value : [-45, -25, -10, 99],
  label : ["위험", "경고", "주의", "관심"]
        var FOOD_POISONING = {
  value : [34, 69, 94, 999],
  label : ["관심", "주의", "경고", "위험"]
}
var FROZEN = {
  value : [25, 50, 75, 100],
  label : ["낮음", "보통", "높음", "매우높음"]
}
var DISCOMFORT = {
  value : [67, 74, 79, 999],
  label : ["낮음", "보통", "높음", "매우높음"]
}
var COLD = {
  value : [0, 1, 2, 3],
  label : ["낮음", "보통", "높음", "매우높음"]
}
var SKIN_DISEASE = {
  value : [0, 1, 2, 3],
  label : ["낮음", "보통", "높음", "매우높음"]
}
var RESPIRATORY_DISEASE = {
  value : [0, 1, 2, 3],
  label : ["낮음", "보통", "높음", "매우높음"]
}
var STROKE = {
  value : [0, 1, 2, 3],
  label : ["낮음", "보통", "높음", "매우높음"]
}
         */

    }
    private void lifeLabel(int val) {
        switch (val) {

        }
    }


    //화면에 보이지 않은 View는파쾨를 해서 메모리를 관리함.
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub

        container.removeView((View)object);

    }

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        // TODO Auto-generated method stub
        return v==((View)obj);
    }

}