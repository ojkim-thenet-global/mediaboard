/*
 * Copyright (c) 2017 LingoChamp Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.onk365.hahaha.view;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.liulishuo.okdownload.DownloadContext;
import com.liulishuo.okdownload.DownloadContextListener;
import com.liulishuo.okdownload.DownloadListener;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.SpeedCalculator;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.cause.ResumeFailedCause;
import com.liulishuo.okdownload.core.listener.DownloadListener1;
import com.liulishuo.okdownload.core.listener.assist.Listener1Assist;

import com.onk365.hahaha.BuildConfig;
import com.onk365.hahaha.R;
import com.onk365.hahaha.base.BaseActivity;
import com.onk365.hahaha.client.ApiClient;
import com.onk365.hahaha.client.ApiClientTest;
import com.onk365.hahaha.client.ApiService;
import com.onk365.hahaha.database.DatabaseHelper;
import com.onk365.hahaha.dialogs.LogoutDialogFragment;
import com.onk365.hahaha.managers.PrefManager;
import com.onk365.hahaha.models.Adver;
import com.onk365.hahaha.models.AdversResponse;
import com.onk365.hahaha.models.MasterResponse;
import com.onk365.hahaha.queue.ProgressUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.liulishuo.okdownload.sample.base.BaseSampleActivity;
//import org.jetbrains.annotations.Nullable;

@SuppressWarnings("LineLength")
public class BunchActivity extends BaseActivity {

    private static final int INDEX_TAG = 1;
    private static final int CURRENT_PROGRESS = 2;
    private String CONTENTS_URL;

    private TextView startOrCancelTv;
    private TextView store_name;
    private View startOrCancelView;
    private View logoutView;

    private View deleteContainerView;
    private RadioButton serialRb;
    private RadioGroup radioGroup;
    private TextView updateInfo;

    private TextView bunchInfoTv;
    private ProgressBar bunchProgressBar;
    private TaskViews[] taskViewsArray;

    private int totalCount;
    private int currentCount;
    private SpeedCalculator speedCalculator;
    //private String[] urls;
    private ArrayList urls = new ArrayList();

    private File bunchDir;
    private DatabaseHelper db;
    private boolean bPermiss =  false;
    private boolean bDownload =  false;

    private DownloadContext downloadContext;
    private DownloadListener listener;
    private final String filePos = Environment.getExternalStorageDirectory().getAbsolutePath() + HAHAHA_FILE;

    private static final String TAG = BunchActivity.class.getSimpleName();
    private String upper;
    private String lower;
    private String survey1_btn;
    private String survey2_btn;
    private String survey3_btn;
    private String survey4_btn;
    PrefManager prefMan;

    private final String APP_VERSION_NAME = BuildConfig.VERSION_NAME;
    private TextView version_info;


    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(BunchActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
            bPermiss = true ;
        }
        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(BunchActivity.this, "Permission Denied" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };

    //public int titleRes() {
    // return R.string.bunch_download_title;
    //}

    private BunchActivity.SendMassgeHandler mMainHandler = null;
    private BunchActivity.CountThread mCountThread = null;
    private static final int SEND_THREAD_INFOMATION = 0;
    private static final int SEND_THREAD_STOP_MESSAGE = 1;
    private int iShowTime = 6;

    // Thread 클래스
    class CountThread extends Thread implements Runnable {

        private boolean isPlay = false;
        public CountThread() {
            isPlay = true;
        }
        public void isThreadState(boolean isPlay) {
            this.isPlay = isPlay;
        }
        public void stopThread() {
            isPlay = !isPlay;
            if (mCountThread!=null) {
                mCountThread=null;
            }
        }
        @Override
        public void run() {
            super.run();
            int i = 0;
            while (isPlay) {
                i++;
                //iCnt++;
                // 메시지 얻어오기
                Message msg = mMainHandler.obtainMessage();
                // 메시지 ID 설정
                msg.what = SEND_THREAD_INFOMATION;
                if(iShowTime == 0) msg.what = SEND_THREAD_STOP_MESSAGE;
                mMainHandler.sendMessage(msg);
                // 1초
                try { Thread.sleep(1000); }
                catch (InterruptedException e) { e.printStackTrace(); }
            }
        }
    }

    // Handler 클래스
    class SendMassgeHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.v("handleMessage", "null");
            switch (msg.what) {

                case SEND_THREAD_INFOMATION:
                    Log.d("11111111111111>", String.valueOf(iShowTime));
                    if(bPermiss) {
                        //5초후 화면전환
                        iShowTime--;
                        if(bDownload) {
                            updateInfo.setText(String.valueOf(iShowTime) + " 초후에 다운로드가 실행 됩니다.");
                        } else {
                            updateInfo.setText(String.valueOf(iShowTime) + " 초후에 슬라이드가 자동 실행 됩니다.");
                        }
                    }

                    if (iShowTime == 0) {

                        if(bDownload) {
                            updateInfo.setText("다운로드 중 입니다.");
                            startOrCancelView.performClick();
                        } else {
                            moveBoard();
                        }

                    }

                    break;
                case SEND_THREAD_STOP_MESSAGE:
                    mCountThread.stopThread();
                    if (mCountThread!=null) {
                        mCountThread=null;
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_bunch);
        setContentView(R.layout.activity_bunch2);
        //setContentView(R.layout.activity_bunch2_test);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        version_info = (TextView) findViewById(R.id.version_info);
        version_info.setText(APP_VERSION_NAME);

        initViews();

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .check();




        //다운로드 경로를 지정
        String savePath = Environment.getExternalStorageDirectory().getAbsolutePath() + HAHAHA_FILE;;
        File dir = new File(savePath);
        //상위 디렉토리가 존재하지 않을 경우 생성
        if (!dir.exists()) {
            dir.mkdirs();
        }
        db = new DatabaseHelper(this);
        prefMan = new PrefManager(this);
        store_name.setText( prefMan.getStoreName() );

        if(prefMan.getServerType().equals("T")) {
            //CONTENTS_URL = "http://hahaha.whitesoft.net/";
            CONTENTS_URL = "http://onk.qwave.co.kr:64080";
        } else {
            //CONTENTS_URL = "http://mb2.taejeongroup.com/";
            //CONTENTS_URL = "http://192.168.0.234:8082/";
            CONTENTS_URL = "http://onk.qwave.co.kr:64080";              // 개발
            //CONTENTS_URL = "http://www.hahaha.care";
            //CONTENTS_URL = "http://file.hahaha.care";
            //CONTENTS_URL = "http://d1sewg9q7f22i8.cloudfront.net";          // Cloud Front
        }

        Log.d(TAG, "##############CONTENTS_URL: " + CONTENTS_URL);

        onMaster();

        // 슬라이드 정보 내려받기
        onSelect(prefMan.getStoreId(), prefMan.getDisplayType());

        bunchDir = new File(filePos);//new File(DemoUtil.getParentFile(this), "bunch");

        initListener();
        initAction();

    }
    private void onSelect(String store_id, String display_type) {


        if(display_type == null || display_type == ""){
            display_type = "A";
        }

        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<AdversResponse> call = apiService.getSlidesList(store_id, display_type);
        call.enqueue(new Callback<AdversResponse>() {
            @Override
            public void onResponse(Call<AdversResponse> call, Response<AdversResponse> response) {
                int statusCode = response.code();

                Log.d(TAG, String.valueOf(statusCode) );
                //List<Adver> advers = response.body().getResults();
                List<Adver> mAdverList = response.body().getSlideList();


                //전체 DB 삭제
                db.deleteAllAdver();
                urls.clear(); //수정사항
                for(int i = 0 ; i < mAdverList.size();  i++) {

                    db.insertAdver(mAdverList.get(i).getCategoryNo(),mAdverList.get(i).getSlideNo(), mAdverList.get(i).getSlideLayout(), mAdverList.get(i).getShowTime(), mAdverList.get(i).getThumbSrc()
                            , mAdverList.get(i).getM0Type(),mAdverList.get(i).getM0Src()
                            , mAdverList.get(i).getM1Type(),mAdverList.get(i).getM1Src()
                            , mAdverList.get(i).getM2Type(),mAdverList.get(i).getM2Src()
                            , mAdverList.get(i).getM3Type(),mAdverList.get(i).getM3Src()
                            , mAdverList.get(i).getM4Type(),mAdverList.get(i).getM4Src()
                            , mAdverList.get(i).getM5Type(),mAdverList.get(i).getM5Src()
                            , mAdverList.get(i).getM6Type(),mAdverList.get(i).getM6Src()
                            , mAdverList.get(i).getM7Type(),mAdverList.get(i).getM7Src()
                            , mAdverList.get(i).getM8Type(),mAdverList.get(i).getM8Src()
                            , mAdverList.get(i).getBGType(),mAdverList.get(i).getBGSrc(), mAdverList.get(i).getEtcVal());
                    //콘텐츠가 있는지 체크
                    //Log.e(TAG, mAdverList.get(i).getThumbSrc().toString());
                    if(!TextUtils.isEmpty(mAdverList.get(i).getThumbSrc())) {
                        addUrls(mAdverList.get(i).getThumbSrc().toString());
                    }
                    /*
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM0Src())) {
                        addUrls(mAdverList.get(i).getM0Src().toString());
                    }*/
                    // 예외처리 - 에디터 타입의 데이터가 이미지일 경우 다운로드
                    // 이미지명이 "editor_mb"로 시작하기 때문에 이미지명으로 구분(ex. editor_mb12354984354.png)
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM0Src())) {
                        if( !TextUtils.isEmpty(mAdverList.get(i).getM0Type()) ) {

                            if( mAdverList.get(i).getM0Type().equals("30") ) {
                                String m0src = mAdverList.get(i).getM0Src().toString();
                                if( m0src.indexOf("editor_mb") > -1 ) {
                                    addUrls(mAdverList.get(i).getM0Src().toString());
                                }
                            }else {
                                addUrls(mAdverList.get(i).getM0Src().toString());
                            }
                        }
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM1Src())) {
                        addUrls(mAdverList.get(i).getM1Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM2Src())) {
                        addUrls(mAdverList.get(i).getM2Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM3Src())) {
                        addUrls(mAdverList.get(i).getM3Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM4Src())) {
                        addUrls(mAdverList.get(i).getM4Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM5Src())) {
                        addUrls(mAdverList.get(i).getM5Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM6Src())) {
                        addUrls(mAdverList.get(i).getM6Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM7Src())) {
                        addUrls(mAdverList.get(i).getM7Src().toString());
                    }
                    if(!TextUtils.isEmpty(mAdverList.get(i).getM8Src())) {
                        addUrls(mAdverList.get(i).getM8Src().toString());
                    }
                    // 추가 배경 이미지
                    if(!TextUtils.isEmpty(mAdverList.get(i).getBGType())) {
                        if(mAdverList.get(i).getBGType().equals("10")) {
                            if( !TextUtils.isEmpty(mAdverList.get(i).getBGSrc()) ) {
                                addUrls(mAdverList.get(i).getBGSrc().toString());
                            }
                        }
                    }

                }
                //푸터정보
                if(!TextUtils.isEmpty(upper)) {
                    addUrls(upper);
                }
                if(!TextUtils.isEmpty(lower)) {
                    addUrls(lower);
                }

                if(!TextUtils.isEmpty(survey1_btn)) {
                    addUrls(survey1_btn);
                }
                if(!TextUtils.isEmpty(survey2_btn)) {
                    addUrls(survey2_btn);
                }
                if(!TextUtils.isEmpty(survey3_btn)) {
                    addUrls(survey3_btn);
                }
                if(!TextUtils.isEmpty(survey4_btn)) {
                    addUrls(survey4_btn);
                }


                //날씨판넬 20190614 수정
                //addUrls("master/weather_bar_1.jpg");
                //addUrls("master/weather_bar_2.jpg");
                //addUrls("master/weather_bar_3.jpg");
                //addUrls("master/weather_bar_4.jpg");
                //addUrls("master/weather_bar_5.jpg");

                int urls_size = urls.size();
                String update_info;
                if(urls_size > 0) {
                    update_info = "총 " + String.valueOf(urls_size) + " 개 의 업데이트가 있습니다.";
                    updateInfo.setText(update_info);

                    bDownload = true ;
                    fnCountDown();

                } else {
                    update_info = "업데이트 내용이 없습니다. ";
                    updateInfo.setText(update_info);
                    //카운트다운 시작
                    //moveBoard();
                    fnCountDown();

                }

            }
            @Override
            public void onFailure(Call<AdversResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                Log.d("tj", "api fail");
            }
        });
    }

    //Master 정보
    private void onMaster() {

        // 20190503 마스터정보 수정
        String store_id = prefMan.getStoreId();


        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<MasterResponse> call = apiService.getMastersList(store_id);
        call.enqueue(new Callback<MasterResponse>() {
            @Override
            public void onResponse(Call<MasterResponse> call, Response<MasterResponse> response) {
                int statusCode = response.code();
                Log.d(TAG, String.valueOf(statusCode) );
                if (statusCode == 200) {

                    String code = response.body().getCode();
                    if (code.equals("100")) {
                        /*surv = response.body().getMaster().getSurvey();

                        Log.d("9999999999999999999999",surv) ;
                        if(!surv.equals("")) {
                            mSurvey.setVisibility(View.VISIBLE);
                        }*/

                        upper = response.body().getMaster().getUpper();
                        lower = response.body().getMaster().getLower();

                        survey1_btn = response.body().getMaster().getSurvey1_btn();
                        survey2_btn = response.body().getMaster().getSurvey2_btn();
                        survey3_btn = response.body().getMaster().getSurvey3_btn();
                        survey4_btn = response.body().getMaster().getSurvey4_btn();

                    } else {
                        ;
                    }
                } else {
                    ;
                }
            }
            @Override
            public void onFailure(Call<MasterResponse> call, Throwable t) {
                Log.e(TAG, t.toString());

                String update_info = " 네트워크가 연결되어 있지 않습니다.";
                updateInfo.setText(update_info);

                fnCountDown();

            }
        });
    }
    private void fnCountDown() {
        //메인 핸들러 생성
        mMainHandler = new BunchActivity.SendMassgeHandler();
        //화면 초기화
        //fnInit();
        if (mCountThread!=null) {
            mCountThread.stopThread();
        }
        mCountThread = new BunchActivity.CountThread();
        mCountThread.start();
    }
    private void addUrls(String url){
        String checkPath;;
        //2. 다운로드 받았는지 확인(파일명은 경로 / 를 _ 로 수정)
        checkPath = filePos + url.toString().replace("/", "_");
        File files = new File(checkPath);
        if(files.exists()==true) {
            //파일이 있을시
        } else {
            //파일이 없을시 다운로드
            url = url;
            urls.add(url) ;
        }
    }

    private void initViews() {
        startOrCancelTv = findViewById(R.id.startOrCancelTv);
        startOrCancelView = findViewById(R.id.startOrCancelView);
        updateInfo = findViewById(R.id.update_info);

        logoutView = findViewById(R.id.logoutView);
        store_name = findViewById(R.id.store_name);


        deleteContainerView = findViewById(R.id.deleteActionView);

        //radioGroup = findViewById(R.id.radioGroup);
        //serialRb = findViewById(R.id.serialRb);

        bunchInfoTv = findViewById(R.id.bunch_info_tv);
        bunchProgressBar = findViewById(R.id.progressBar);
        taskViewsArray = new TaskViews[3];
        taskViewsArray[0] = new TaskViews(this, R.id.pb1_info_tv, R.id.progressBar1);
        taskViewsArray[1] = new TaskViews(this, R.id.pb2_info_tv, R.id.progressBar2);
        taskViewsArray[2] = new TaskViews(this, R.id.pb3_info_tv, R.id.progressBar3);
        //taskViewsArray[3] = new TaskViews(this, R.id.pb4_info_tv, R.id.progressBar4);
        //taskViewsArray[4] = new TaskViews(this, R.id.pb5_info_tv, R.id.progressBar5);
    }

    private void initListener() {
        listener = new DownloadListener1() {

            @Override
            public void taskStart(@NonNull DownloadTask task,
                                  @NonNull Listener1Assist.Listener1Model model) {
                fillPbInfo(task, "start");
                Log.d("tj", "start : " + task);
            }

            @Override
            public void retry(@NonNull DownloadTask task, @NonNull ResumeFailedCause cause) {
                fillPbInfo(task, "retry");
            }

            @Override
            public void connected(@NonNull DownloadTask task, int blockCount, long currentOffset,
                                  long totalLength) {
                fillPbInfo(task, "connected");
                Log.d("tj", "connected : " + task + " / " + blockCount);
            }

            @Override
            public void progress(@NonNull DownloadTask task, long currentOffset, long totalLength) {
                calcSpeed(task, currentOffset);

                fillPbInfo(task, "progress");
                // progress
                final int id = task.getId();

                Log.d("tj", "progress : " + task.getTag() + " / " + task);


                task.addTag(CURRENT_PROGRESS, currentOffset);
                final TaskViews taskViews = findValidTaskViewsWithId(id);
                if (taskViews == null) return;
                final ProgressBar progressBar = taskViews.progressBar;
                if (progressBar == null) return;
                ProgressUtil.calcProgressToViewAndMark(progressBar, currentOffset, totalLength);
            }

            @Override public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause,
                                          @android.support.annotation.Nullable Exception realCause,
                                          @NonNull Listener1Assist.Listener1Model model) {
                fillPbInfo(task, "end " + cause);

                currentCount += 1;
                updateBunchInfoAndProgress();

                releaseTaskViewsWithId(task.getId());
            }
        };
    }

    private void initAction() {
        startOrCancelView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(final View v) {

                //카운트다운 안되게
                bPermiss = false;

                if(urls.size() > 0) {
                    if (v.getTag() == null) {
                        // start
                        final long startTime = SystemClock.uptimeMillis();
                        v.setTag(new Object());
                        final DownloadContext.Builder builder = new DownloadContext.QueueSet()
                                .setParentPathFile(bunchDir)
                                .setMinIntervalMillisCallbackProcess(300)
                                .commit();
                        Log.d("BunchActivity", "before bind bunch task consume "
                                + (SystemClock.uptimeMillis() - startTime) + "ms");
                        String fileName;
                        for (int i = 0; i < urls.size(); i++) {
                            fileName = urls.get(i).toString().replace("/", "_");

                            //builder.bind(CONTENTS_URL + "admin/upload/" + urls.get(i).toString(), fileName).addTag(INDEX_TAG, i);

                            // 리소스 다운로드 주소
                            builder.bind(CONTENTS_URL + "/crm/crm_s3_dev/mbContents/" + urls.get(i).toString(), fileName).addTag(INDEX_TAG, i);       // 개발 서버
                            //builder.bind(CONTENTS_URL + "/mbContents/" + urls.get(i).toString(), fileName).addTag(INDEX_TAG, i);            // file.hahaha

                            Log.d("tj", "다운로드 목록 : " + i + " ## " + urls.get(i).toString());

                            //http://www.hahaha.care/crm_s3/mbContents/common/1/1/1/img_0.jpg
                            // 20190614 수정
                            // http://file.hahaha.care/mbContents/common/31/1/1/img_0.jpg -> http://d1sewg9q7f22i8.cloudfront.net/mbContents/common/27/28/1/thumbnail.png
                        }

                        totalCount = urls.size();
                        currentCount = 0;

                        Log.d("BunchActivity", "before build bunch task consume "
                                + (SystemClock.uptimeMillis() - startTime) + "ms");
                        downloadContext = builder.setListener(new DownloadContextListener() {
                            @Override
                            public void taskEnd(@NonNull DownloadContext context,
                                                @NonNull DownloadTask task,
                                                @NonNull EndCause cause,
                                                @Nullable Exception realCause,
                                                int remainCount) {
                                Log.d("tj","$$$$$$$$$ task1 : " + task);
                                Log.d("tj","$$$$$$$$$ Exception : " + realCause + " / " + cause + " / " + context.isStarted() + " / " + remainCount);
                            }

                            @Override
                            public void queueEnd(@NonNull DownloadContext context) {
                                v.setTag(null);
                                //radioGroup.setEnabled(true);
                                deleteContainerView.setEnabled(true);
                                startOrCancelTv.setText(R.string.start);
                                Toast.makeText(BunchActivity.this, "다운로드가 완료되었습니다.", Toast.LENGTH_SHORT).show();
                                moveBoard();
                            }
                        }).build();

                        speedCalculator = new SpeedCalculator();
                        Log.d("BunchActivity", "before bunch task consume "
                                + (SystemClock.uptimeMillis() - startTime) + "ms");
                        //downloadContext.start(listener, serialRb.isChecked());
                        downloadContext.start(listener, false);
                        deleteContainerView.setEnabled(false);
                        //radioGroup.setEnabled(false);
                        startOrCancelTv.setText(R.string.cancel);
                        updateInfo.setText("다운로드 중 입니다.");// 수정사항
                        Log.d("BunchActivity",
                                "start bunch task consume " + (SystemClock
                                        .uptimeMillis() - startTime) + "ms");
                    } else {
                        // stop
                        downloadContext.stop();
                    }
                } else {

                    moveBoard();
                }
            }
        });

        deleteContainerView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                //카운트다운 안되게
                bPermiss = false;
                showDeleteDialog();

            }
        });
        logoutView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                showlogoutDialog();
            }
        });

    }
    private void moveBoard(){

        if (mCountThread!=null) {
            mCountThread.stopThread();
            mCountThread=null;
        }

        Intent intent = new Intent(BunchActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void fillPbInfo(DownloadTask task, String info) {
        final int index = (int) task.getTag(INDEX_TAG);
        final TaskViews taskViews = findValidTaskViewsWithId(task.getId());
        if (taskViews == null) return;
        taskViews.infoTv.setText("Task: " + index + " [" + info + "]");
    }

    private void updateBunchInfoAndProgress() {
        bunchInfoTv.setText(
                "Total Progress: " + currentCount + "/" + totalCount + "(" + speedCalculator
                        .speed() + ")");
        bunchProgressBar.setMax(totalCount);
        bunchProgressBar.setProgress(currentCount);
    }

    private void calcSpeed(DownloadTask task, long currentOffset) {
        final Object progressValue = task.getTag(CURRENT_PROGRESS);
        final long preOffset = progressValue == null ? 0 : (long) progressValue;
        final long increase = currentOffset - preOffset;
        speedCalculator.downloading(increase);

        updateBunchInfoAndProgress();
    }


    private static class TaskViews {
        final ProgressBar progressBar;
        final TextView infoTv;

        int id = 0;

        TaskViews(Activity activity, @IdRes int tvId, @IdRes int pbId) {
            progressBar = activity.findViewById(pbId);
            infoTv = activity.findViewById(tvId);
        }
    }

    TaskViews findValidTaskViewsWithId(int taskId) {
        for (TaskViews taskViews : taskViewsArray) {
            if (taskViews.id == taskId) return taskViews;
        }

        for (TaskViews taskViews : taskViewsArray) {
            if (taskViews.id == 0) {
                taskViews.id = taskId;
                return taskViews;
            }
        }

        return null;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountThread!=null) {
            mCountThread.stopThread();
            mCountThread=null;
        }
    }

    void releaseTaskViewsWithId(int taskId) {
        for (TaskViews taskViews : taskViewsArray) {
            if (taskViews.id == taskId) taskViews.id = 0;
        }
    }
    private void showlogoutDialog() {
        new LogoutDialogFragment().show(this.getSupportFragmentManager(), "logout-dialog");
    }
    private void showDeleteDialog(){ //수정사항
        new AlertDialog.Builder(BunchActivity.this)
                .setTitle("Confirm Delete")
                .setMessage("삭제하시겠습니까")
                .setCancelable(false)
                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String[] children = bunchDir.list();
                        if (children != null) {
                            for (String child : children) {
                                new File(bunchDir, child).delete();
                            }
                        }
                        bunchDir.delete();
                        onSelect(prefMan.getStoreId(), prefMan.getDisplayType());
                        startOrCancelTv.setText("다운로드 하기");
                        // 미디어보드 고도화 추가

                    }
                })
                .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onSelect(prefMan.getStoreId(), prefMan.getDisplayType());
                        startOrCancelTv.setText("다운로드 하기");
                    }
                }).show();
    }
}
