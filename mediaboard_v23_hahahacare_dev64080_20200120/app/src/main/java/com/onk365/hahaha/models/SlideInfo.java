package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class SlideInfo {

    @SerializedName("display_code")
    private String display_code;

    public String getDisplayCode() {
        return display_code;
    }
    public void setDisplayCode(String display_code) {
        this.display_code = display_code;
    }

}