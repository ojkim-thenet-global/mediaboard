package com.onk365.hahaha.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewSwitcher;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.onk365.hahaha.BuildConfig;
import com.onk365.hahaha.R;
import com.onk365.hahaha.base.BaseActivity;
import com.onk365.hahaha.client.ApiClient;
import com.onk365.hahaha.client.ApiClientTest;
import com.onk365.hahaha.client.ApiService;
import com.onk365.hahaha.database.DatabaseHelper;
import com.onk365.hahaha.dialogs.SurveyDialogFragment;
import com.onk365.hahaha.dialogs.ThumbDialogFragment;
import com.onk365.hahaha.managers.PrefManager;
import com.onk365.hahaha.models.Adver;
import com.onk365.hahaha.models.MasterResponse;
import com.onk365.hahaha.models.UpdateResponse;
import com.onk365.hahaha.models.Weathers;
import com.onk365.hahaha.models.WeathersResponse;
import com.onk365.hahaha.utils.BackPressCloseHandler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends  BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private BackPressCloseHandler backPressCloseHandler;
    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    PrefManager prefMan;

    private int CurrentPosition = 0;
    private int iShowTime = 10;
    private int iInitShowTime = 6;
    private int iBnr = 0;
    private boolean bWeather = false;
    private boolean bFirst = true;
    private boolean bBackground = false;
    private int iRefreshTime = 0;
    // 미디어보드 고도화 추가 날씨정보
    private int iWeatherTimeFail = 300;
    private int iWeatherTimeSuccess = 3600;
    private int iWeatherTime = iWeatherTimeSuccess;


    // 미디어보드 고도화 추가 - 미디어보드 슬라이드 컨텐츠 업데이트 타임
    private int iRefreshAdverTime = 0;

    // 마지막 슬라이드가 동영상 or 링크이고, 첫번째 슬라이드가 동영상 or 링크일 때 롤링배너 처리를 위한 플래그
    private boolean lastSlideVideo = false;

    private SendMassgeHandler mMainHandler = null;
    private CountThread mCountThread = null;

    private static final int SEND_THREAD_INFOMATION = 0;
    private static final int SEND_THREAD_STOP_MESSAGE = 1;

    private View currView;
    private VideoView videoView;
    private VideoView youtubeView;
    private ImageView imageView;
    private MediaController mediaController;
    private String conType = "10";
    private String survey1_btn;
    private String survey1_url;
    private String survey2_btn;
    private String survey2_url;
    private String survey3_btn;
    private String survey3_url;
    private String survey4_btn;
    private String survey4_url;

    private List<Adver> adversList = new ArrayList<>();
    private List<WeathersResponse> weathersList = new ArrayList<>();
    private List<UpdateResponse> updateList = new ArrayList<>();
    private DatabaseHelper db;
    private Adver adver;

    private ImageButton mList;
    private ImageButton mSurvey1;
    private ImageButton mSurvey2;
    private ImageButton mSurvey3;
    private ImageButton mSurvey4;

    private TextView dateNow; //수정사항
    private TextView dateNow1; //수정사항

    Animation animSlideUp;
    Animation animSlideDown, animSlideDownHead, animSlideUpFooter;
    Animation slide_in_up;

    RelativeLayout header;
    RelativeLayout footer;
    ImageView top;
    ImageView lower;

    ViewSwitcher mViewSwitcher;
    Animation slide_in_right, slide_out_left;

    private String master_upper;
    private Inflater inflater;

    public static Context context;


    // Thread 클래스
    class CountThread extends Thread implements Runnable {

        private boolean isPlay = false;

        public CountThread() {
            isPlay = true;
        }

        public void isThreadState(boolean isPlay) {
            this.isPlay = isPlay;
        }

        public void stopThread() {
            isPlay = !isPlay;
            if (mCountThread != null) {
                mCountThread = null;
            }
        }

        @Override
        public void run() {
            super.run();
            int i = 0;
            while (isPlay) {
                i++;
                //iCnt++;
                // 메시지 얻어오기
                Message msg = mMainHandler.obtainMessage();
                // 메시지 ID 설정
                msg.what = SEND_THREAD_INFOMATION;
                //if(iAudioplay == 3) msg.what = SEND_THREAD_STOP_MESSAGE;
                //if(iAudioplay == 8) msg.what = SEND_THREAD_STOP_MESSAGE;
                mMainHandler.sendMessage(msg);
                // 1초
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Handler 클래스
    class SendMassgeHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.v("handleMessage", "null");
            switch (msg.what) {
                case SEND_THREAD_INFOMATION:
                    //백그라운드 Check
                    fnBackCheck();
                    //백그라운드 모드일때는 ...정지
                    if (!bBackground) {
                        iShowTime--;
                        iRefreshTime++;

                        // 미디어보드 고도화 추가
                        iRefreshAdverTime++;

                        if (conType.equals("20")) {
                            //iBnr++;
                            //if(iBnr <= 0) {
                            //fnNextBnr();
                            //}
                        }
                        if (conType.equals("21")) {
                            iBnr++;
                            fnNextBnr2();
                        }

                        Log.d("===============>", String.valueOf(iShowTime));

                        if (iShowTime <= 0) {
                            fnNext();
                        }

                        // 미디어보드 고도화 추가 - 슬라이드 컨텐츠 업데이트 타임
                        // 300 -> 5분, 600 -> 10분
                        if (iRefreshAdverTime > 60) {
                            //onSelect(prefMan.getStoreId());

                            String store_id = prefMan.getStoreId();
                            //int store_seq = prefMan.getStoreSeq();

                            String datetime = prefMan.getDateTime();

                            String display_code = prefMan.getDisplayCode();
                            String display_type = prefMan.getDisplayType();

                            getSlideRefreshInfo(store_id, datetime, display_type, display_code);

                            //Toast.makeText(MainActivity.this, "슬라이드 업데이트 : " + display_code + " ## "+ display_type + " ## " + datetime +" ## "+ iRefreshAdverTime,  Toast.LENGTH_LONG).show();
                            iRefreshAdverTime = 0;
                        }
                    }
                    break;
                case SEND_THREAD_STOP_MESSAGE:
                    mCountThread.stopThread();
                    if (mCountThread != null) {
                        mCountThread = null;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        dateNow = (TextView) findViewById(R.id.dateNow); //수정사항
        dateNow1 = (TextView) findViewById(R.id.dateNow1); //수정사항

        ShowTimeMethod();


        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        boolean isImmersiveModeEnabled = ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            Log.i("Is on?", "Turning immersive mode mode off. ");
        } else {
            Log.i("Is on?", "Turning immersive mode mode on.");
        }
        // 몰입 모드를 꼭 적용해야 한다면 아래의 3가지 속성을 모두 적용시켜야 합니다
        newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);

        // 미디어보드 고도화 추가 (0번째 포지션에서 ViewPagerAdapter.java 페이지에서 함수 불러올 때 사용)
        context = this;

        prefMan = new PrefManager(this);

        backPressCloseHandler = new BackPressCloseHandler(this);

        mViewSwitcher = (ViewSwitcher) findViewById(R.id.view_switcher);
        slide_in_right = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_right);
        slide_out_left = AnimationUtils.loadAnimation(this,
                R.anim.slide_out_left);
        slide_in_up = AnimationUtils.loadAnimation(this,
                R.anim.slide_in_up);

        mViewSwitcher.setInAnimation(slide_in_right);
        mViewSwitcher.setOutAnimation(slide_out_left);

        //INTRO 광고
        db = new DatabaseHelper(this);
        Adver adver = db.getAdver(36);

        String m0type = adver.getM0Type();
        String m0src = adver.getM0Src().replace("/", "_");
        ImageView imageView = (ImageView) findViewById(R.id.image_view);
        File c = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + m0src);
        Glide.with(this).load(c).into(imageView);

        //날씨정보
        onSelect(prefMan.getStoreId());

        animSlideDownHead = AnimationUtils.loadAnimation(this, R.anim.slide_down);
        animSlideUpFooter = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        animSlideUp = AnimationUtils.loadAnimation(this, R.anim.slide_in_up);
        animSlideDown = AnimationUtils.loadAnimation(this, R.anim.slide_in_down);


        mList = (ImageButton) findViewById(R.id.btnList);
        mList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showThumbDialog();
            }
        });

        mSurvey1 = (ImageButton) findViewById(R.id.btnSurvey1);
        mSurvey1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSurveyDialog("1");
            }
        });
        //mSurvey1.setVisibility(View.INVISIBLE);
        mSurvey1.setVisibility(View.GONE);


        mSurvey2 = (ImageButton) findViewById(R.id.btnSurvey2);
        mSurvey2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSurveyDialog("2");
            }
        });
        //mSurvey2.setVisibility(View.INVISIBLE);
        mSurvey2.setVisibility(View.GONE);


        mSurvey3 = (ImageButton) findViewById(R.id.btnSurvey3);
        mSurvey3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSurveyDialog("3");
            }
        });
        //mSurvey3.setVisibility(View.INVISIBLE);
        mSurvey3.setVisibility(View.GONE);

        mSurvey4 = (ImageButton) findViewById(R.id.btnSurvey4);
        mSurvey4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSurveyDialog("4");
            }
        });
        mSurvey4.setVisibility(View.GONE);


        header = (RelativeLayout) findViewById(R.id.header);
        footer = (RelativeLayout) findViewById(R.id.footer);

        top = (ImageView) findViewById(R.id.top);
        lower = (ImageView) findViewById(R.id.lower);


        //푸터 배너정보
        onMaster();

        //메인 핸들러 생성
        mMainHandler = new SendMassgeHandler();
        //화면 초기화
        //fnInit();
        if (mCountThread != null) {
            mCountThread.stopThread();
        }
        mCountThread = new CountThread();
        mCountThread.start();


        // 플레이 스토어 버전 체크
        //getMargetVersion version = new getMargetVersion();
        //version.execute();

    }

    public void ShowTimeMethod() { //수정사항
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Date rightNow=new Date();
                SimpleDateFormat formatter = new SimpleDateFormat(
                        "yyyy년 MM월 dd일 ");
                String dateString=formatter.format(rightNow);
                dateNow.setText(dateString);

                Date rightNow1=new Date();
                SimpleDateFormat formatter1 = new SimpleDateFormat(
                        "hh:mm:ss");
                String dateString1=formatter1.format(rightNow1);
                dateNow1.setText(dateString1);

               /* Date rightNow1=new Date();
                SimpleDateFormat formatter1 = new SimpleDateFormat(
                        "hh:mm ");
                String dateString1=formatter1.format(rightNow1);
                dateNow1.setText(dateString1);*/
            }
        };
        Runnable task = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    handler.sendEmptyMessage(1);
                }
            }
        };
        Thread thread = new Thread(task);
        thread.start();
    }


    private void showInit() {
        adversList = new ArrayList<>();

        RelativeLayout header = (RelativeLayout) findViewById(R.id.header);
        RelativeLayout header2 = (RelativeLayout) findViewById(R.id.header2);

        if(bWeather) {
            //setContentView(R.layout.activity_main);
            header.setVisibility(View.VISIBLE);
            header2.setVisibility(View.INVISIBLE);
            header.startAnimation(animSlideDownHead);
            displayWeatherBanner();

        } else {
            header.setVisibility(View.INVISIBLE);
            header2.setVisibility(View.VISIBLE);
            header2.startAnimation(animSlideDownHead);

        }
        adversList.addAll(db.getAllAdvers(bWeather));
        footer.startAnimation(animSlideUpFooter);

        //intro slide 광고 노출 시간
        //iInitShowTime = adversList.get(0).getShowTime();
        //makeSlide
        makeSlide();

    }

    private void showThumbDialog() {
        Bundle args = new Bundle();
        args.putBoolean("weather", bWeather);
        ThumbDialogFragment dialogFragment = new ThumbDialogFragment();
        dialogFragment.setArguments(args);
        dialogFragment.show(this.getSupportFragmentManager(), "thumb-dialog");
    }

    // 버튼 클릭
    private void showSurveyDialog(String type) {
        Bundle args = new Bundle();

        if (type.equals("1")) {
            args.putString("surveyurl", survey1_url);
        }else if (type.equals("2")) {
            args.putString("surveyurl", survey2_url);
        }else if (type.equals("3")) {
            args.putString("surveyurl", survey3_url);
        }else if (type.equals("4")) {
            args.putString("surveyurl", survey4_url);
        }
        args.putString("type", type);

        SurveyDialogFragment dialogFragment = new SurveyDialogFragment();
        dialogFragment.setArguments(args);
        dialogFragment.show(this.getSupportFragmentManager(), "survey-dialog");
    }


    private void makeSlide(){

        //if(adversList.size() > 0) {
        iShowTime = adversList.get(0).getShowTime();

        //SLIDE 만들기
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPagerAdapter = new ViewPagerAdapter(getLayoutInflater(), adversList, weathersList, master_upper, this);
        //mViewPagerAdapter = new ViewPagerAdapter(getLayoutInflater(), adversList, weathersList, this);

        mViewPager.setAdapter(mViewPagerAdapter);
        //View currView;
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            boolean first = true;

            //아이템이 변경되면
            @Override public void onPageSelected(int position) {
                CurrentPosition = position;

                if(position == 1) {
                }

                if(mediaController != null) {
                    mediaController = null;
                }


                adver = adversList.get(position);
                conType = adver.getM0Type();
                //Log.d("conType", conType);

                //String m0src = adver.getM0Src().replace("/", "_");
                //광고 노출 시간
                iShowTime = adver.getShowTime();
                iBnr = 0;

                currView = mViewPager.getFocusedChild();


                // 이미지 슬라이드
                if(conType.equals("10") && position > 0) {
                    //imageView= (ImageView) currView.findViewById(R.id.image_view);
                    //imageView.startAnimation(animSlideDown);

                }



                if(conType.equals("20") || conType.equals("22") || conType.equals("24") || conType.equals("26")) {



                    //if( (conType.equals("20"))  || (conType.equals("21")) ) {


                    if ( (adversList.size() - 1) == CurrentPosition ) {
                        lastSlideVideo = true;
                    } else {
                        lastSlideVideo = false;
                    }



                    String layout_num = "";
                    if(conType.equals("20")){
                        layout_num = "20";

                    }else if(conType.equals("22")){
                        layout_num = "22";

                    }else if(conType.equals("24")){
                        layout_num = "24";

                    }else if(conType.equals("26")){
                        layout_num = "26";
                    }


                    String tag = layout_num + CurrentPosition;
                    View view = mViewPager.findViewWithTag(tag);

                    //videoView = (VideoView) currView.findViewById(R.id.video_view);
                    videoView = (VideoView) view.findViewById(R.id.video_view);
                    mediaController = new MediaController(MainActivity.this);
                    mediaController.setVisibility(View.GONE);
                    //mediaController.setAnchorView(view.findViewById(R.id.media_layout));
                    //mediaController.setPadding(0,50,0,20);
                    videoView.setMediaController(mediaController);
                    //videoView.requestFocus();

                    //videoView.setVideoURI(Uri.parse("http://vod.hantalk.net/2/hotel21.mp4"));

                    videoView.seekTo(0);
                    videoView.start();

                    // rolling Banner
                    if(conType.equals("20")){
                        fnNextBnr();
                    }



                } else if(conType.equals("21") || conType.equals("23") || conType.equals("25") || conType.equals("27")) {
                    // video end

                    if ( (adversList.size() - 1) == CurrentPosition ) {
                        lastSlideVideo = true;
                    } else {
                        lastSlideVideo = false;
                    }


                    String m0src = adver.getM0Src();
                    //m0src = "https://www.youtube.com/embed/AsXxuIdpkWM";
                    //m0src  = "http://onk.qwave.co.kr:64080/crm/crm_s3_dev/mbContents/common/30/11/1/mov_0.mp4";


                    // 태그 번호
                    String layout_num = "";


                    // 유튜브인지 아닌지 판단
                    int youtube_flag = m0src.indexOf("youtube");

                    if(youtube_flag > -1) {



                        if(conType.equals("21")){
                            layout_num = "21";

                        }else if(conType.equals("23")){
                            layout_num = "23";

                        }else if(conType.equals("25")){
                            layout_num = "25";

                        }else if(conType.equals("27")){
                            layout_num = "27";
                        }

                        //View view = mViewPager.findViewWithTag("21");
                        String tag = layout_num + CurrentPosition;
                        View view = mViewPager.findViewWithTag(tag);
                        WebView webView = (WebView) view.findViewById(R.id.youtube_view);


                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
                        webView.requestFocusFromTouch();
                        webView.setWebChromeClient(new WebChromeClient());
                        // 캐시 비활성화
                        //webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                        //webView.loadUrl(m0src);


                        // rolling Banner
                        if(conType.equals("21")){
                            fnNextBnr2();
                        }


                    }else{

                        if(conType.equals("21")){
                            layout_num = "20";

                        }else if(conType.equals("23")){
                            layout_num = "22";

                        }else if(conType.equals("25")){
                            layout_num = "24";

                        }else if(conType.equals("27")){
                            layout_num = "26";
                        }

                        String tag = layout_num + CurrentPosition;
                        View view = mViewPager.findViewWithTag(tag);
                        VideoView videoView = (VideoView) view.findViewById(R.id.video_view);
                        mediaController = new MediaController(MainActivity.this);
                        mediaController.setVisibility(View.GONE);
                        //mediaController.setAnchorView(view.findViewById(R.id.media_layout));
                        //mediaController.setPadding(0,50,0,20);
                        videoView.setMediaController(mediaController);
                        //videoView.requestFocus();


                        //videoView.setVideoURI(Uri.parse("http://vod.hantalk.net/2/hotel21.mp4"));

                        videoView.seekTo(0);
                        videoView.start();


                        // rolling Banner
                        if(conType.equals("21")){
                            fnNextBnr2();
                        }

                    }


                } else {
                    // 예외처리
                    String tag = "10000" + CurrentPosition;
                    View view = mViewPager.findViewWithTag(tag);

                }

            }
            @Override public void onPageScrolled(int position, float positionOffest, int positionOffsetPixels) {
                if (first && positionOffest == 0 && positionOffsetPixels == 0){
                    onPageSelected(0);
                    first = false;
                }
            }
            @Override public void onPageScrollStateChanged(int state) {
            }
        });
    }


    //Master 정보
    private void onMaster() {

        // 20190503 마스터정보 수정
        String store_id = prefMan.getStoreId();

        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<MasterResponse> call = apiService.getMastersList(store_id);
        call.enqueue(new Callback<MasterResponse>() {
            @Override
            public void onResponse(Call<MasterResponse> call, Response<MasterResponse> response) {
                int statusCode = response.code();
                Log.d(TAG, String.valueOf(statusCode) );
                if (statusCode == 200) {

                    String code = response.body().getCode();
                    if (code.equals("100")) {

                        String upper = response.body().getMaster().getUpper().replace("/", "_");
                        master_upper = upper;


                        survey1_btn = response.body().getMaster().getSurvey1_btn();
                        survey1_url = response.body().getMaster().getSurvey1_url();

                        survey2_btn = response.body().getMaster().getSurvey2_btn();
                        survey2_url = response.body().getMaster().getSurvey2_url();

                        survey3_btn = response.body().getMaster().getSurvey3_btn();
                        survey3_url = response.body().getMaster().getSurvey3_url();

                        survey4_btn = response.body().getMaster().getSurvey4_btn();
                        survey4_url = response.body().getMaster().getSurvey4_url();

                        Log.d("9999999999999999999999",survey1_url) ;

                        // 폴더 경로
                        String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
                        String surv = "";
                        String pathName = "";

                        if( !survey1_url.equals("") && !survey1_btn.equals("") ) {

                            surv = survey1_btn.replace("/", "_");
                            pathName = baseDir + "/hahaha/" + surv;

                            Bitmap bmp = BitmapFactory.decodeFile(pathName);
                            mSurvey1.setImageBitmap(bmp);

                            mSurvey1.setVisibility(View.VISIBLE);
                        }
                        if( !survey2_url.equals("") && !survey2_btn.equals("") ) {

                            surv = survey2_btn.replace("/", "_");
                            pathName = baseDir + "/hahaha/" + surv;

                            Bitmap bmp = BitmapFactory.decodeFile(pathName);
                            mSurvey2.setImageBitmap(bmp);

                            mSurvey2.setVisibility(View.VISIBLE);

                        }
                        if( !survey3_url.equals("") && !survey3_btn.equals("") ) {

                            surv = survey3_btn.replace("/", "_");
                            pathName = baseDir + "/hahaha/" + surv;

                            Bitmap bmp = BitmapFactory.decodeFile(pathName);
                            mSurvey3.setImageBitmap(bmp);

                            mSurvey3.setVisibility(View.VISIBLE);
                        }
                        if( !survey4_url.equals("") && !survey4_btn.equals("") ) {

                            surv = survey4_btn.replace("/", "_");
                            pathName = baseDir + "/hahaha/" + surv;

                            Bitmap bmp = BitmapFactory.decodeFile(pathName);
                            mSurvey4.setImageBitmap(bmp);

                            mSurvey4.setVisibility(View.VISIBLE);
                        }


                        String slower = response.body().getMaster().getLower().replace("/", "_");
                        File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/" + slower);
                        Glide.with(lower.getContext()).load(c).into(lower);


                    } else {
                        ;
                    }
                } else {
                    ;
                }
            }
            @Override
            public void onFailure(Call<MasterResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }


    //날씨정보
    private void onSelect(String store_id) {
        int nx = prefMan.getNX();
        int ny = prefMan.getNY();
        String address_pref = prefMan.getAddressPref();
        String address_county = prefMan.getAddressCounty();
        String areaNo = prefMan.getAreaNo();
        String systemCharacter = "utf8";
        bWeather = false;

        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        //Call<WeathersResponse> call = apiService.getWeatherList(nx, ny, address_pref, address_county, areaNo);
        Call<WeathersResponse> call = apiService.getWeatherList(nx, ny, address_pref, address_county, areaNo, systemCharacter);
        call.enqueue(new Callback<WeathersResponse>() {
            @Override
            public void onResponse(Call<WeathersResponse> call, Response<WeathersResponse> response) {
                int statusCode = response.code();
                Log.d(TAG, String.valueOf(statusCode) );
                if (statusCode == 200) {

                    String code = response.body().getCode();
                    if (code.equals("100")) {

                        bWeather = true;
                        weathersList = new ArrayList<>();
                        weathersList.add(response.body());
                        //Log.d(TAG, "자외선 received: " + response.body().getSkin().getToday());
                        //displayWeatherBanner(response.body());
                        //Log.d(TAG, String.valueOf(response.body().getWeather().getSKY()) );


                        // SKY(하늘), PTY(강수) 둘 중 하나라도 못 받아오면 fail 처리
                        if(response.body().getWeather().getSKY() == 999 || response.body().getWeather().getPTY() == 999 ) {
                            bWeather = false;

                            Toast.makeText(MainActivity.this, "날씨정보조회-오류", Toast.LENGTH_SHORT).show();

                            iRefreshTime = 0;
                            iWeatherTime = iWeatherTimeFail;

                        } else if (response.body().getPart().getPm25Value().equals("999") && response.body().getPart().getPm10Value().equals("999") && response.body().getUv().getToday().equals("999") ) {

                            // Pm25Value(초미세먼지), Pm10Value(미세먼지), Uv(자외선) 모두 못 받아오면 fail 처리
                            bWeather = false;

                            Toast.makeText(MainActivity.this, "날씨정보조회-오류", Toast.LENGTH_SHORT).show();

                            iRefreshTime = 0;
                            iWeatherTime = iWeatherTimeFail;

                        } else {
                            Toast.makeText(MainActivity.this, "날씨정보조회", Toast.LENGTH_SHORT).show();

                            iRefreshTime = 0;
                            iWeatherTime = iWeatherTimeSuccess;
                        }
                        /*
                        if(response.body().getCold().equals(null)) {
                            bWeather = false;
                            Toast.makeText(MainActivity.this, "날씨정보조회-오류", Toast.LENGTH_SHORT).show();

                        }*/


                    } else {
                        Toast.makeText(MainActivity.this, "날씨정보를 가져오지 못했습니다.", Toast.LENGTH_SHORT).show();
                        iRefreshTime = 0;
                        iWeatherTime = iWeatherTimeFail;
                    }
                } else {
                    Toast.makeText(MainActivity.this, "인터넷이 연결되어 있지 않습니다.", Toast.LENGTH_SHORT).show();
                    iRefreshTime = 0;
                    iWeatherTime = iWeatherTimeFail;
                }
                //List<Weather> mAdverList = response.body().getWeather();
            }
            @Override
            public void onFailure(Call<WeathersResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
                iRefreshTime = 0;
                iWeatherTime = iWeatherTimeFail;
            }
        });
    }


    // 상단 날씨 배너 표시
    private void displayWeatherBanner() {
        Weathers.Weather weather = weathersList.get(0).getWeather();

        TextView t1h = (TextView) findViewById(R.id.t1h);
        TextView TMN = (TextView) findViewById(R.id.TMN);
        TextView TMX = (TextView) findViewById(R.id.TMX);

        TextView txtUv = (TextView) findViewById(R.id.uv);
        TextView txtPart = (TextView) findViewById(R.id.part);
        TextView txtPart2 = (TextView) findViewById(R.id.part2);
        TextView txtWind = (TextView) findViewById(R.id.wind);

        // 기상 실황
        //var weather = data.weather;
        if (weather != null ) {
            int sky = weather.getSKY();
            int pty = weather.getPTY();

            //int images[] = {R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4};

            // 하늘, 강수형태로 이미지 선택
            if (sky == 999 || pty == 999){
            }else{
                if (pty == 2 || pty == 3) {
                    //$(weatherBanner).css("background-image", "url(img/weather_bar_5.jpg)");
                    //header.setBackgroundResource(R.drawable.weather_bar_5);
//                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_5.jpg");
//                Glide.with(top.getContext()).load(c).into(top);

                    // 배너 임시 수정 2019-01-03
                    top.setImageResource(R.drawable.master_weather_bar_5);
                } else if (pty == 1) {
                    //$(weatherBanner).css("background-image", "url(img/weather_bar_4.jpg)");
                    //header.setBackgroundResource(R.drawable.weather_bar_4);
//                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_4.jpg");
//                Glide.with(top.getContext()).load(c).into(top);
                    top.setImageResource(R.drawable.master_weather_bar_4);
                } else if (sky == 3 || sky == 4) {
                    //$(weatherBanner).css("background-image", "url(img/weather_bar_3.jpg)");
                    //header.setBackgroundResource(R.drawable.weather_bar_3);
//                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_3.jpg");
//                Glide.with(top.getContext()).load(c).into(top);
                    top.setImageResource(R.drawable.master_weather_bar_3);
                } else if (sky == 2) {
                    //$(weatherBanner).css("background-image", "url(img/weather_bar_2.jpg)");
                    //header.setBackgroundResource(R.drawable.weather_bar_2);
//                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_2.jpg");
//                Glide.with(top.getContext()).load(c).into(top);
                    top.setImageResource(R.drawable.master_weather_bar_2);
                } else {
                    //$(weatherBanner).css("background-image", "url(img/weather_bar_1.jpg)");
                    //header.setBackgroundResource(R.drawable.weather_bar_1);
//                File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_1.jpg");
//                Glide.with(top.getContext()).load(c).into(top);
                    top.setImageResource(R.drawable.master_weather_bar_1);
                }

                // 현재 온도
                //if (weather.getT1H() ) {
                //$(curTemperature).html(makeTemperatureLabel(Math.round(weather.T1H)));
                //} else {
                //    $(curTemperature).html("-");
                //}

                if (weather.getT1H() == 999.0 || weather.getT1H().equals("-")) {
                    t1h.setText(" - ");
                } else {

                    double d = weather.getT1H();
                    d = Math.round(d);
                    int i = (int) d;
                    t1h.setText(String.valueOf(i));
                }

            }


        } else {

            // 날씨가 없는 경우 모든 값을 비우고 기본 배너를 표시
            //$(curTemperature).html("");
            //$(lowTemperature).html("");
            //$(highTemperature).html("");
            //$(particulate).html("");
            //$(wind_chill).html("");
            //$(uv_ray).html("");
            //$(weatherBanner).css("background-image", "url(img/top.jpg)");
            //header.setBackgroundResource(R.drawable.top);
            //File c = new File(Environment.getExternalStorageDirectory() + "/hahaha/master_weather_bar_1.jpg");
            Glide.with(top.getContext()).load(R.drawable.top).into(top);
        }

        // 최저 온도
        Weathers.Low low = weathersList.get(0).getLow();
        Weathers.High high = weathersList.get(0).getHigh();

        /*var low = data.low;
        if (low && low.hasOwnProperty('TMN')) {
            $(lowTemperature).html(makeTemperatureLabel(low.TMN));
        } else {
            $(lowTemperature).html("-");
        }*/

        if(low.getTMN() != 999 || String.valueOf(low.getTMN()).equals("-")) {
            TMN.setText(String.valueOf(low.getTMN()));
        } else {
            TMN.setText(" - ");
        }

        // 최고 온도
        /*var high = data.high;
        if (high && high.hasOwnProperty('TMX')) {
            $(highTemperature).html(makeTemperatureLabel(high.TMX));
        } else {
            $(highTemperature).html("-");
        }*/

        if(high.getTMX() != 999 || String.valueOf(low.getTMN()).equals("-")) {
            TMX.setText(String.valueOf(high.getTMX()));
        } else {
            TMX.setText(" - ");
        }


        /*
        .blue-font { color: #32c1db; }
.green-font { color: #59d24d; }
.orange-font { color: #ff852c; }
.red-font { color: #f2414b; }
         */

        // 주소 - 상단 날씨 배너 수정 2019-01-02
        String address_pref_banners = prefMan.getAddressPref();
        String address_county_banners = prefMan.getAddressCounty();
        TextView address_tv = (TextView) findViewById(R.id.address);
        String address =  address_county_banners + " 기준";
        address_tv.setText(address);

        // 미세먼지
        Weathers.Part part = weathersList.get(0).getPart();

        String label = "미측정";
        String color = "#32c1db";
        //var part = data.part;
        //if (part && part.hasOwnProperty('pm10Value')) {
        if(!TextUtils.isEmpty(part.getPm10Value()) && !part.getPm10Value().equals("-") && !part.getPm10Value().equals("999")) {
            int val = Integer.parseInt(part.getPm10Value());
            label = "좋음";
            if (val > 150) {
                //cls = "red-font";
                color = "#f2414b";
                label = "매우나쁨";
            } else if (val > 80) {
                //cls = "orage-font";
                color = "#ff852c";
                label = "나쁨";
            } else if (val > 30) {
                color = "#59d24d";
                //cls = "green-font";
                label = "보통";
            }
            // 좋음 0 ~ 30

            label = label + " " +String.valueOf(val) +  " ㎍/㎡";
            // 데이터가 있을때만 setText()
            txtPart.setTextColor(Color.parseColor(color));
            txtPart.setText(label);

        } else {
            //label = "좋음 - ㎍/㎡";
            txtPart.setText("미측정");
            txtPart.setTextColor(Color.parseColor("#626262"));
        }

        //$(particulate).html("<span class='" + cls + "'>" + label + " " + val + " ㎍/㎡</span>");

        //} else {
        //    $(particulate).html("-");
        //}




        // 초미세먼지
        label = "미측정";
        if(!TextUtils.isEmpty(part.getPm25Value()) && !part.getPm25Value().equals("-") && !part.getPm25Value().equals("999")) {
            int val = Integer.parseInt(part.getPm25Value());
            label = "좋음";
            if (val > 75) {
                //cls = "red-font";
                color = "#f2414b";
                label = "매우나쁨";
            } else if (val > 35) {
                //cls = "orage-font";
                color = "#ff852c";
                label = "나쁨";
            } else if (val > 15) {
                color = "#59d24d";
                //cls = "green-font";
                label = "보통";
            }
            // 좋음 0 ~ 15

            label = label + " " +String.valueOf(val) +  " ㎍/㎡";
            // 데이터가 있을때만 setText()
            txtPart2.setTextColor(Color.parseColor(color));
            txtPart2.setText(label);

        } else {
            //label = "좋음 - ㎍/㎡";
            txtPart2.setText("미측정");
            txtPart2.setTextColor(Color.parseColor("#626262"));
        }





        // ==================== 2019-01-02 날씨 상단 배너 수정 ==========================================
//        Calendar now = Calendar.getInstance(Locale.KOREA);
//        int month2 = now.get(Calendar.MONTH) + 1;
        Calendar calendar = new GregorianCalendar(Locale.KOREA);
        int month = calendar.get(Calendar.MONTH) + 1;

        LinearLayout uvLayout = (LinearLayout) findViewById(R.id.uv_layout);
        LinearLayout windLayout = (LinearLayout) findViewById(R.id.wind_layout);

        // 자외선 4월 ~ 11월 / 체감온도 12월 ~ 3월
        if (month >= 4 && month <= 11) {
            uvLayout.setVisibility(View.VISIBLE);  // 보임
            windLayout.setVisibility(View.GONE); // 안보임

            // 자외선
            Weathers.Uv uv = weathersList.get(0).getUv();

            //Log.d("자외선------------", uv.getToday());
            label = "미측정";
            color = "";
            if(!TextUtils.isEmpty(uv.getToday())&& !uv.getToday().equals("-") && !uv.getToday().equals("999")) {
                int val = Integer.parseInt(uv.getToday());

                if (2 >= val)
                    color = "#3B8EFD";
                label = "낮음";
                if (2 < val && val <= 5)
                    color = "#59d24d";
                label = "보통";
                if (5 < val && val <= 7)
                    color = "#ff852c";
                label = "높음";
                if (7 < val && val <= 10)
                    color = "#f2414b";
                label = "매우높음";
                if (10 < val && val <= 99) {
                    color = "#930000";
                    label = "위험";
                }
                txtUv.setText(label);
                txtUv.setTextColor(Color.parseColor(color));
            } else {
                txtUv.setTextColor(Color.parseColor("#626262"));
                txtUv.setText("미측정");
            }

        /*var uv = data.uv;
        if (uv && uv.hasOwnProperty('today')) {
            $(uv_ray).html("<span class='green-font'>" + makeLifeLabel(uv.today, UV_RAY) + "</span>");
        } else {
            $(uv_ray).html("<span class='green-font'>" + UV_RAY.label[0] + "</span>");
        }
        */

            // 12월 ~ 3월
        } else {

            windLayout.setVisibility(View.VISIBLE);  // 보임
            uvLayout.setVisibility(View.GONE); // 레이아웃 위치 까지 안보임

            // 체감온도
            label = "미측정";
            Weathers.Wind wind = weathersList.get(0).getWind();
            if(!TextUtils.isEmpty(wind.getToday()) && !wind.getToday().equals("-") && !wind.getToday().equals("999")) {
                int val = Integer.parseInt(wind.getToday());
                // 여기 수정
//                label = label + String.valueOf(val) +  " ℃";
                label = String.valueOf(val) +  " ℃";
                txtWind.setText(label);
                txtUv.setTextColor(Color.parseColor("#3B8EFD"));

            } else {
                txtUv.setTextColor(Color.parseColor("#626262"));
                txtWind.setText("미측정");
            }
            /*var wind = data.wind;
            if (wind && wind.hasOwnProperty('h3')) {
                $(wind_chill).html("<span class='blue-font'>" + wind.h3 + "℃</span>");
            } else {
                $(wind_chill).html("-");
            }*/
        }

    }

    public void fnMove(int position){
        mViewPager.setCurrentItem(position);
    }

    // 레이아웃2 동영상+이미지+롤링배너(6개)
    private void fnNextBnr(){


        if (lastSlideVideo && CurrentPosition == 0) return;


        String tag = "20" + CurrentPosition;
        View view = mViewPager.findViewWithTag(tag);

        String bnr1 ="";String bnr2 ="";String bnr3 ="";String bnr4 ="";String bnr5 ="";String bnr6 ="";
        //ImageView image_bnr  = (ImageView) currView.findViewById(R.id.image_bnr);
        ImageView image_bnr  = (ImageView) view.findViewById(R.id.image_bnr);


        int time = Integer.parseInt( adver.getEtcVal() );
        if(time == 0) time = 5;

        //콘텐츠가 있는지 체크
        if(iBnr == 0) {

            if (!TextUtils.isEmpty(adver.getM2Src())) {
                bnr2 = adver.getM2Src().replace("/", "_");
                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time) ) {
            if (!TextUtils.isEmpty(adver.getM3Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM3Src().replace("/", "_");
                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideDown);
            }
        }
        if(iBnr == (time*2)) {
            if (!TextUtils.isEmpty(adver.getM4Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM4Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time*3)) {
            if (!TextUtils.isEmpty(adver.getM5Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM5Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideDown);
            }
        }
        if(iBnr == (time*4)) {
            if (!TextUtils.isEmpty(adver.getM6Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM6Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time*5)) {
            if (!TextUtils.isEmpty(adver.getM6Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM7Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }

    }


    // 레이아웃2 동영상+이미지+롤링배너(6개)
    private void fnNextBnr2(){

        if (lastSlideVideo && CurrentPosition == 0) return;

        String m0src = adver.getM0Src();
        //m0src = "https://www.youtube.com/embed/AsXxuIdpkWM";
        //m0src  = "http://onk.qwave.co.kr:64080/crm/crm_s3_dev/mbContents/common/30/11/1/mov_0.mp4";

        int youtube_flag = m0src.indexOf("youtube");

        View view = null;
        String tag = "";
        if(youtube_flag > -1) {
            tag = "21" + CurrentPosition;
            view = mViewPager.findViewWithTag(tag);
        }else {
            tag = "20" + CurrentPosition;
            view = mViewPager.findViewWithTag(tag);
        }

        String bnr1 ="";String bnr2 ="";String bnr3 ="";String bnr4 ="";String bnr5 ="";String bnr6 ="";
        //ImageView image_bnr  = (ImageView) currView.findViewById(R.id.image_bnr);
        ImageView image_bnr  = (ImageView) view.findViewById(R.id.image_bnr);



        int time = Integer.parseInt( adver.getEtcVal() );
        if(time == 0) time = 5;

        //콘텐츠가 있는지 체크
        if(iBnr == 0) {

            if (!TextUtils.isEmpty(adver.getM2Src())) {
                bnr2 = adver.getM2Src().replace("/", "_");
                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time) ) {
            if (!TextUtils.isEmpty(adver.getM3Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM3Src().replace("/", "_");
                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideDown);
            }
        }
        if(iBnr == (time*2)) {
            if (!TextUtils.isEmpty(adver.getM4Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM4Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time*3)) {
            if (!TextUtils.isEmpty(adver.getM5Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM5Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideDown);
            }
        }
        if(iBnr == (time*4)) {
            if (!TextUtils.isEmpty(adver.getM6Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM6Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }
        if(iBnr == (time*5)) {
            if (!TextUtils.isEmpty(adver.getM6Src())) {
                //image_bnr.startAnimation(animSlideUp);
                bnr2 = adver.getM7Src().replace("/", "_");

                File d = new File(Environment.getExternalStorageDirectory() + HAHAHA_FILE + bnr2);
                Glide.with(image_bnr.getContext()).load(d).into(image_bnr);
                //image_bnr.startAnimation(animSlideUp);
                image_bnr.startAnimation(animSlideUp);
            }
        }

    }


    private void fnBackCheck(){
        bBackground = isAppIsInBackground(this);
        Log.d("=====background=====",String.valueOf(bBackground));
    }
    private void fnNext(){
        Log.d("current", String.valueOf(CurrentPosition)) ;
        Log.d("current", String.valueOf(adversList.size())) ;


        if(bFirst) {
            //첫 페이지에서는 다음페이지(슬라이드로 이동)
            showInit();
            mViewSwitcher.showNext();
            bFirst = false;
            //iShowTime = 10;

        } else {
            //다음페이지
            if (CurrentPosition < adversList.size() -1) {
                //mViewPager.setCurrentItem(CurrentPosition + 1);
                mViewPager.setCurrentItem(CurrentPosition+1, true);
            } else {

                Log.d("first", "첫페이지로 이동") ;
                mViewSwitcher.showPrevious();


                iShowTime = iInitShowTime;
                //첫 페이지로 이동
                bFirst = true;

                CurrentPosition = 0;
                //mViewPager.setCurrentItem(0, false);
                // 3600 2019-01-04 1시간 -> 3시간 수정
                // 10800 2019-01-14 3시간 -> 50분 수정
                // 3000 2019-05-09 50분 -> 날씨정보 오류 시 5분 수정
                if (iRefreshTime > iWeatherTime) {

                    //String minute = getMinute();
                    //if(minute.equals("45")){}

                    // 날씨정보 : 1시간 이상 지나면 업데이트
                    onSelect(prefMan.getStoreId());
                    iRefreshTime = 0;
                }

                /*
                if(bWeather) {
                    // 3600 2019-01-04 1시간 -> 3시간 수정
                    // 10800 2019-01-14 3시간 -> 50분 수정

                    if (iRefreshTime > 3000) {
                        //날씨정보 : 1시간 이상 지나면 업데이트
                        onSelect(prefMan.getStoreId());
                        iRefreshTime = 0;
                    }
                }*/

            }
        }
    }
    private int getItemofviewpager(int i) {
        return mViewPager.getCurrentItem() + i;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountThread!=null) {
            mCountThread.stopThread();
            mCountThread=null;
        }
    }

    @Override
    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }



    // Glide 라이브러리 이미지 리플래시가 안 될 때 clear
    private void setGlideView(Context context, File file, ImageView iv){
        Glide.clear(iv);
        Glide.with(context).load(file).signature(new StringSignature(UUID.randomUUID().toString())).into(iv);
    }



    // 슬라이드 업데이트 체크
    private void getSlideRefreshInfo(String store_seq, String datetime, String display_type, String display_code){
        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }

        Call<UpdateResponse> call = apiService.getCheckSlide(store_seq, datetime, display_type, display_code);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    String code = response.body().getCode();

                    if (code.equals("100")) {


                        String slideRefresh = response.body().getSlideRefresh();

                        if(slideRefresh.equals("Y")) {
                            //Toast.makeText(MainActivity.this, "편성표 업데이트! " + iRefreshAdverTime, Toast.LENGTH_SHORT).show();
                            Toast.makeText(MainActivity.this, "편성표를 업데이트합니다 ", Toast.LENGTH_SHORT).show();

                            // 현재 날짜를 저장
                            prefMan.setDateTime(getDateTime());

                            // 업데이트 시
                            Intent intent;
                            intent = new Intent(MainActivity.this, CheckActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                    }
                } else {
                    Log.d(TAG, "##############Number of user ERROR: " + statusCode);
                }
            }
            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }



    // 현재 시간을 가져옴
    public String getDateTime() {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        String datetime = format.format(date);

        return datetime;
    }

    // 현재 시간(분)을 가져옴
    public String getMinute() {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("mm");
        String datetime = format.format(date);
        return datetime;
    }




    // 플레이스토어 추가
    String marketVersion, verSion;
    AlertDialog.Builder mDialog;


    private class getMargetVersion extends AsyncTask<Void, Void, String> {

        private AppCompatActivity appCompatActivity = new AppCompatActivity();
        private final String APP_VERSION_NAME = BuildConfig.VERSION_NAME;
        private final String APP_PACKAGE_NAME = BuildConfig.APPLICATION_ID;

        // nexon를 예를 들었습니다.
        //private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.nexon.axe";
        //private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.onk365.hahaha&hl=ko&ah=_Rq8YOKpu4v0MKczVpk3H-CzJW0";

        //private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.oderstation.android";
        private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.onk365.hahaha";

        //private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.sundaytoz.astovekr.joy";
        //private final String STORE_URL = "https://play.google.com/store/apps/details?id=com.neowizgames.game.dot";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {

            try{
                Document doc = Jsoup.connect(STORE_URL).get();

                Elements Version = doc.select(".htlgb");

                for (int i = 0; i < Version.size(); i++) {

                    String VersionMarket = Version.get(i).text();

                    if (Pattern.matches("^[0-9]{1}.[0-9]{1}.[0-9]{1}$", VersionMarket)) {

                        return VersionMarket;
                    }
                    /*
                    if(Pattern.matches("^[0-9]{1}.[0-9]{1}$", VersionMarket)) {

                        return VersionMarket;
                    }*/


                    Log.d("playstore", "playstore market version: " + VersionMarket);

                }
            }catch(IOException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            //Log.d("playstore", "onPostExecute : " + s +" ## "+ APP_VERSION_NAME);

            if(s != null) {
                if (!s.equals(APP_VERSION_NAME)) { //APP_VERSION_NAME는 현재 앱의

                    Log.d("playstore", "playstore version? " + s);

                    int s_version = Integer.parseInt(s.replace(".", ""));
                    int app_version = Integer.parseInt(APP_VERSION_NAME.replace(".", ""));

                    if (s_version > app_version) {

                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("업데이트 필요!")
                                .setMessage("프로그램을 최신 버전으로 업데이트 해주셔야 사용 가능합니다!")
                                .setCancelable(false)
                                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                try {
                                                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(STORE_URL)));
                                                } catch (android.content.ActivityNotFoundException anfe) {
                                                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(STORE_URL)));
                                                }
                                                MainActivity.this.finish();
                                            }
                                        });
                                    }
                                })
                                .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                Toast.makeText(MainActivity.this, "프로그램을 종료합니다!", Toast.LENGTH_SHORT).show();
                                                MainActivity.this.finish();

                                            }
                                        });
                                    }
                                })
                                .show();

                    /*
                    mDialog.setMessage("최신 버전이 출시되었습니다. 업데이트 후 사용 가능합니다.")
                            .setCancelable(false)
                            .setPositiveButton("업데이트 바로가기",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int id) {
                                            Intent marketLaunch = new Intent(
                                                    Intent.ACTION_VIEW);
                                            marketLaunch.setData(Uri
                                                    .parse(STORE_URL));
                                            startActivity(marketLaunch);
                                            finish();
                                        }
                                    });
                    AlertDialog alert = mDialog.create();
                    alert.setTitle("업데이트 알림");
                    alert.show();
                    */
                    }
                }
            }
            super.onPostExecute(s);
        }
    }

}
