package com.onk365.hahaha.dialogs;

import android.app.Dialog;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.onk365.hahaha.R;
import com.onk365.hahaha.database.DatabaseHelper;
import com.onk365.hahaha.managers.PrefManager;
import com.onk365.hahaha.models.Adver;
import com.onk365.hahaha.utils.MyDividerItemDecoration;
import com.onk365.hahaha.utils.RecyclerTouchListener;
import com.onk365.hahaha.view.FirstTimeActivity;
import com.onk365.hahaha.view.MainActivity;
import com.onk365.hahaha.view.NotesAdapter;

import java.util.ArrayList;
import java.util.List;

public class ThumbDialogFragment extends DialogFragment {

    Context context;
    PrefManager prefMan;
    private RecyclerView recyclerView;
    private NotesAdapter mAdapter;
    private List<Adver> notesList = new ArrayList<>();

    private DatabaseHelper db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        prefMan = new PrefManager(context);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        //Display display = getWindowManager().getDefaultDisplay();

        //Window window = dialog.getWindow();
        //window.setLayout(10000, 3000 );

        //dialog.getWindow().setLayout(2000, 300);
        dialog.getWindow().setGravity(Gravity.BOTTOM);


        // dp 사이즈 변환
        int margin_value = 144;
        DisplayMetrics dm = getResources().getDisplayMetrics();
        int size = Math.round(margin_value * dm.density);

        // margin 적용
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.bottomMargin = size;

        ViewGroup.LayoutParams attributes = dialog.getWindow().getAttributes();
        ((WindowManager.LayoutParams) attributes).y = size;


        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_thumb, null);

        recyclerView = view.findViewById(R.id.recycler_view);
        Bundle mArgs = getArguments();

        //날씨정보 유무
        boolean bWeather = mArgs.getBoolean("weather");

       // Button mOk = (Button) view.findViewById(R.id.button1);
       // Button mCancel = (Button) view.findViewById(R.id.button2);

        db = new DatabaseHelper(context);

        notesList.addAll(db.getAllAdvers(bWeather));
        mAdapter = new NotesAdapter(context, notesList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.addItemDecoration(new MyDividerItemDecoration(context, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(mAdapter);
        //recyclerView.scrollToPosition(notesList.size() - 1);



        DisplayMetrics dm = getResources().getDisplayMetrics();
        int size = Math.round(100 * dm.density);

        LinearLayout thumb_layout = (LinearLayout) view.findViewById(R.id.thumb_layout);
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        param.bottomMargin = size;
        thumb_layout.setLayoutParams(param);



        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showActions(position);
            }

            //@Override
            public void onLongClick(View view, int position) {
                showActions(position);
            }
        }));
/*
        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //prefMan.logout();
                //startActivity(new Intent(getActivity(), FirstTimeActivity.class));
                //getActivity().finish();
                dismiss();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
*/
        return view;
    }
    private void showActions(final int position) {
        //Toast.makeText(context, "선택한 광고로 이동합니다.", Toast.LENGTH_SHORT).show();
        MainActivity callingActivity = (MainActivity) getActivity();
        callingActivity.fnMove(position);
        dismiss();

    }
}
