package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class Version {
    @SerializedName("code")
    private String code;
    @SerializedName("version")
    private String version;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getVerion() { return version; }
    public void setVersion(String version) {   this.version = version;}


}