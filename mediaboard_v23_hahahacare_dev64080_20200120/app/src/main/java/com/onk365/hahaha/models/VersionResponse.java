package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

public class VersionResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("version")
    private Version version;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public Version getVersion() { return version; }
    public void setVersion(Version version) {   this.version = version;}

}