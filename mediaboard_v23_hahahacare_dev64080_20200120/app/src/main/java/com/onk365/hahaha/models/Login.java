package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class Login {
    @SerializedName("code")
    private String code;
    @SerializedName("storeInfo")
    private StoreInfo storeInfo;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public StoreInfo getStoreInfo() { return storeInfo; }
    public void setStoreInfo(StoreInfo storeInfo) {   this.storeInfo = storeInfo;}


}