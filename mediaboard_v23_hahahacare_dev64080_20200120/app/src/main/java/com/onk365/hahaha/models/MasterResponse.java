package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

public class MasterResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("masterDataInfo")
    private MasterDataInfo masterDataInfo;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public MasterDataInfo getMaster() { return masterDataInfo; }
    public void setMaster(MasterDataInfo masterDataInfo) {   this.masterDataInfo = masterDataInfo;}

}