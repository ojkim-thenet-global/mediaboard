package com.onk365.hahaha.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoRun extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // 배터리 교체 등으로 부팅시 앱 자동실행 시키기
        String action = intent.getAction();
        if(action.equals("android.intent.action.BOOT_COMPLETED")){
            Intent ii = new Intent(context, CheckActivity.class);
            ii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(ii);
        }
    }
}
