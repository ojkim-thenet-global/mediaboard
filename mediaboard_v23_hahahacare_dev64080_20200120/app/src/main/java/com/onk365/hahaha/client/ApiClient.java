package com.onk365.hahaha.client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onk365.hahaha.base.BaseActivity;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient extends BaseActivity{

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }
        return retrofit;

    }
}