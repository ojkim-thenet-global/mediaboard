package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class MasterDataInfo {
    @SerializedName("upper")
    private String upper;
    @SerializedName("lower")
    private String lower;
    @SerializedName("survey1_btn")
    private String survey1_btn;
    @SerializedName("survey1_url")
    private String survey1_url;

    @SerializedName("survey2_btn")
    private String survey2_btn;
    @SerializedName("survey2_url")
    private String survey2_url;

    @SerializedName("survey3_btn")
    private String survey3_btn;
    @SerializedName("survey3_url")
    private String survey3_url;

    @SerializedName("survey4_btn")
    private String survey4_btn;
    @SerializedName("survey4_url")
    private String survey4_url;

    public String getUpper() {
        return upper;
    }
    public void setUpper(String upper) {
        this.upper = upper;
    }
    public String getLower() {
        return lower;
    }
    public void setLower(String lower) {
        this.lower = lower;
    }

    public String getSurvey1_btn() {
        return survey1_btn;
    }
    public void setSurvey1_btn(String survey1_btn) {
        this.survey1_btn = survey1_btn;
    }
    public String getSurvey1_url() {
        return survey1_url;
    }
    public void setSurvey1_url(String survey1_url) {
        this.survey1_url = survey1_url;
    }

    public String getSurvey2_btn() {
        return survey2_btn;
    }
    public void setSurvey2_btn(String survey2_btn) {
        this.survey2_btn = survey2_btn;
    }
    public String getSurvey2_url() {
        return survey2_url;
    }
    public void setSurvey2_url(String survey2_url) {
        this.survey2_url = survey2_url;
    }

    public String getSurvey3_btn() {
        return survey3_btn;
    }
    public void setSurvey3_btn(String survey3_btn) {
        this.survey3_btn = survey3_btn;
    }
    public String getSurvey3_url() {
        return survey3_url;
    }
    public void setSurvey3_url(String survey3_url) {
        this.survey3_url = survey3_url;
    }

    public String getSurvey4_btn() {
        return survey4_btn;
    }
    public void setSurvey4_btn(String survey4_btn) {
        this.survey4_btn = survey4_btn;
    }
    public String getSurvey4_url() {
        return survey4_url;
    }
    public void setSurvey4_url(String survey4_url) {
        this.survey4_url = survey4_url;
    }

}


/*
@SerializedName("upper")
    private String upper;
    @SerializedName("lower")
    private String lower;
    @SerializedName("survey")
    private String survey;

    public String getUpper() {
        return upper;
    }
    public void setUpper(String upper) {
        this.upper = upper;
    }
    public String getLower() {
        return lower;
    }
    public void setLower(String lower) {
        this.lower = lower;
    }
    public String getSurvey() {
        return survey;
    }
    public void setSurvey(String survey) {
        this.survey = survey;
    }
 */