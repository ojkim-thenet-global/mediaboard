package com.onk365.hahaha.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PrefManager {
    //public static final String PREF_API_KEY = "pref_api_key";

    public static final String STORE_ID = "store_id";
    public static final String STORE_SEQ = "store_seq";
    public static final String STORE_NO = "store_no";
    public static final String STORE_NAME = "store_name";

    // 미디어보드 고도화 추가
    public static final String DISPLAY_TYPE = "display_type";
    public static final String DISPLAY_CODE = "display_code";
    public static final String DATE_TIME = "date_time";

    private static Context context;
    private static SharedPreferences prefs;
    private static SharedPreferences.Editor prefeditor;
    private static SharedPreferences reviewsPrefs;
    private static SharedPreferences.Editor reviewsPrefsEditor;

    public PrefManager(Context context) {
        this.context = context;
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefeditor = prefs.edit();
        reviewsPrefs = context.getSharedPreferences("review-lessons_prefs", Context.MODE_PRIVATE);
        reviewsPrefsEditor = reviewsPrefs.edit();
    }
    /*
    public String getApiKey() {
        return prefs.getString("api_key", "0");
    }

    public void setApiKey(String key) {
        prefeditor.putString("api_key", key).commit();
    }
    */

    public boolean isFirstLaunch() {
        return prefs.getBoolean("first_launch", true);
    }
    public void setFirstLaunch(boolean value) {
        prefeditor.putBoolean("first_launch", value).commit();
    }

    public boolean isIdSave() {
        return prefs.getBoolean("first_save", true);
    }
    public void setIdSave(boolean value) {
        prefeditor.putBoolean("first_save", value).commit();
    }

    public String getStoreId() {
        return prefs.getString(STORE_ID, "");
    }
    public void setStoreId(String key) {
        prefeditor.putString(STORE_ID, key).commit();
    }

    public String getStoreNo() {
        return prefs.getString(STORE_NO, "");
    }
    public void setStoreNo(String key) {
        prefeditor.putString(STORE_NO, key).commit();
    }

    public String getStoreName() {
        return prefs.getString(STORE_NAME, "HAHAHA");
    }
    public void setStoreName(String key) {
        prefeditor.putString(STORE_NAME, key).commit();
    }


    public String getServerType() {
        return prefs.getString("server_type", "R");
    }
    public void setServerType(String key) {
        prefeditor.putString("server_type", key).commit();
    }


    // 미디어보드 고도화 추가
    public int getStoreSeq() {
        return prefs.getInt(STORE_SEQ, 0);
    }
    public void setStoreSeq(int key) {
        prefeditor.putInt(STORE_SEQ, key).commit();
    }

    public String getDisplayType() {
        return prefs.getString(DISPLAY_TYPE, "");
    }
    public void setDisplayType(String key) {
        prefeditor.putString(DISPLAY_TYPE, key).commit();
    }

    public String getDisplayCode() {
        return prefs.getString(DISPLAY_CODE, "");
    }
    public void setDisplayCode(String key) {
        prefeditor.putString(DISPLAY_CODE, key).commit();
    }

    public String getDateTime() {
        return prefs.getString(DATE_TIME, "");
    }
    public void setDateTime(String key) {
        prefeditor.putString(DATE_TIME, key).commit();
    }






    /*
String nx = "59";
        String ny = "127";
        String address_pref = "서울";
        String address_county = "마포구";
        String areaNo = "1144000000";
 */
    public int getNX() { return prefs.getInt("nx", 0); }
    public void setNX(int key) { prefeditor.putInt("nx", key).commit(); }
    public int getNY() { return prefs.getInt("ny", 0); }
    public void setNY(int key) { prefeditor.putInt("ny", key).commit(); }
    public String getAddressPref() {
        return prefs.getString("address_pref", "");
    }
    public void setAddressPref(String key) {
        prefeditor.putString("address_pref", key).commit();
    }
    public String getAddressCounty() {
        return prefs.getString("address_country", "");
    }
    public void setAddressCounty(String key) { prefeditor.putString("address_country", key).commit(); }
    public String getAreaNo() {
        return prefs.getString("area_no", "");
    }
    public void setAreaNo(String key) {
        prefeditor.putString("area_no", key).commit();
    }



    /*
    public boolean isProfileFirstTime() {
        return prefs.getBoolean("first_time_profile", true);
    }

    public void setProfileFirstTime(boolean value) {
        prefeditor.putBoolean("first_time_profile", value).commit();
    }
    public boolean isLegendLearned() {
        return PrefManager.prefs.getBoolean("pref_legend_learned", false);
    }
    */

    public void logout() {
        //prefeditor.clear().commit();
        //reviewsPrefsEditor.clear().commit();
        setFirstLaunch(true);

        /*File offlineData = new File(Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/shared_prefs/offline_data.xml");
        File cacheDir = new File(Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/cache");
        File webviewCacheDir = new File(Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/app_webview");

        try {
            if (offlineData.exists()) {
                offlineData.delete();
            }
            FileUtils.deleteDirectory(cacheDir);
            FileUtils.deleteDirectory(webviewCacheDir);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        /*
        // Cancel the notification alarm...
        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                context,
                NotificationPublisher.REQUEST_CODE,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        */
    }

    public static enum Keyboard {
        LOCAL_IME, NATIVE
    }
}
