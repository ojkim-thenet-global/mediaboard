package com.onk365.hahaha.models;
import com.google.gson.annotations.SerializedName;

public class Weathers {


    public class Weather {
        @SerializedName("SKY")
        private int SKY;
        @SerializedName("PTY")
        private int PTY;
        @SerializedName("T1H")
        private double T1H;
        public int getSKY() {
            return SKY;
        }
        public void setSKY(int SKY) {
            this.SKY = SKY;
        }
        public int getPTY() {
            return PTY;
        }
        public void setPTY(int PTY) {   this.PTY = PTY;}
        public Double getT1H() {
            return T1H;
        }
        public void setT1H(double T1H) {
            this.T1H = T1H;
        }
    }

    public class Low {
        @SerializedName("TMN")
        private int TMN;
        public int getTMN() {
            return TMN;
        }
        public void setTMN(int TMN) {
            this.TMN = TMN;
        }
    }
    public class High {
        @SerializedName("TMX")
        private int TMX;
        public int getTMX() {
            return TMX;
        }
        public void setTMX(int TMX) {
            this.TMX = TMX;
        }
    }
    public class Uv {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    /*public class Wind {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }*/
    public class Wind {
        @SerializedName("h3")
        private String h3;
        public String getToday() {
            return h3;
        }
        public void setToday(String h3) {
            this.h3 = h3;
        }
    }
    public class Food {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    public class Frozen {
        @SerializedName("h3")
        private String h3;
        public String getH3() {
            return h3;
        }
        public void setH3(String h3) {
            this.h3 = h3;
        }
    }
    public class Discomfort {
        @SerializedName("h3")
        private String h3;
        public String getH3() {
            return h3;
        }
        public void setH3(String h3) {
            this.h3 = h3;
        }
    }
    public class Cold {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    public class Skin {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    public class Respiratory {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    public class Stroke {
        @SerializedName("today")
        private String today;
        public String getToday() {
            return today;
        }
        public void setToday(String today) {
            this.today = today;
        }
    }
    public class Part {
        @SerializedName("pm10Value")
        private String pm10Value;

        @SerializedName("pm25Value")
        private String pm25Value;

        public String getPm10Value() {
            return pm10Value;
        }
        public void setPm10Value(String pm10Value) {
            this.pm10Value = pm10Value;
        }

        public String getPm25Value() {
            return pm25Value;
        }
        public void setPm25Value(String pm25Value) {
            this.pm25Value = pm25Value;
        }
    }
    
}


