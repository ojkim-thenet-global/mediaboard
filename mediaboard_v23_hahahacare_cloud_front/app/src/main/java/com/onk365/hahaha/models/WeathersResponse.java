package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

public class WeathersResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("weather")
    private Weathers.Weather weather;

    @SerializedName("low")
    private Weathers.Low low;

    @SerializedName("high")
    private Weathers.High high;
    @SerializedName("uv")
    private Weathers.Uv uv;
    @SerializedName("wind")
    private Weathers.Wind wind;
    @SerializedName("food")
    private Weathers.Food food;
    @SerializedName("frozen")
    private Weathers.Frozen frozen;
    @SerializedName("discomfort")
    private Weathers.Discomfort discomfort;
    @SerializedName("cold")
    private Weathers.Cold cold;
    @SerializedName("skin")
    private Weathers.Skin skin;
    @SerializedName("respiratory")
    private Weathers.Respiratory respiratory;
    @SerializedName("stroke")
    private Weathers.Stroke stroke;
    @SerializedName("part")
    private Weathers.Part part;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public Weathers.Weather getWeather() {
        return weather;
    }
    public void setWeather(Weathers.Weather weather) {
        this.weather = weather;
    }

    public Weathers.Low getLow() {
        return low;
    }
    public void setLow(Weathers.Low low) {
        this.low = low;
    }

    public Weathers.High getHigh() {
        return high;
    }
    public void setHigh(Weathers.High high) {
        this.high = high;
    }
    public Weathers.Uv getUv() {
        return uv;
    }
    public void setUv(Weathers.Uv uv) {
        this.uv = uv;
    }
    public Weathers.Wind getWind() {
        return wind;
    }
    public void setWind(Weathers.Wind wind) {
        this.wind = wind;
    }
    public Weathers.Food getFood() {
        return food;
    }
    public void setFood(Weathers.Food food) {
        this.food = food;
    }
    public Weathers.Frozen getFrozen() {
        return frozen;
    }
    public void setFrozen(Weathers.Frozen frozen) {
        this.frozen = frozen;
    }
    public Weathers.Discomfort getDiscomfort() {
        return discomfort;
    }
    public void setDiscomfort(Weathers.Discomfort discomfort) { this.discomfort = discomfort; }
    public Weathers.Cold getCold() {
        return cold;
    }
    public void setCold(Weathers.Cold cold) {
        this.cold = cold;
    }
    public Weathers.Skin getSkin() {
        return skin;
    }
    public void setSkin(Weathers.Skin skin) {
        this.skin = skin;
    }
    public Weathers.Respiratory getRespiratory() {
        return respiratory;
    }
    public void setRespiratory(Weathers.Respiratory respiratory) {
        this.respiratory = respiratory;
    }
    public Weathers.Stroke getStroke() {
        return stroke;
    }
    public void setStroke(Weathers.Stroke stroke) {
        this.stroke = stroke;
    }
    public Weathers.Part getPart() {
        return part;
    }
    public void setPart(Weathers.Part part) {
        this.part = part;
    }

}