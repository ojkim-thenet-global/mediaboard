package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("slideRefresh")
    private String slideRefresh;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public String getSlideRefresh() {
        return slideRefresh;
    }
    public void setSlideRefresh(String slideRefresh) {
        this.slideRefresh = slideRefresh;
    }
}