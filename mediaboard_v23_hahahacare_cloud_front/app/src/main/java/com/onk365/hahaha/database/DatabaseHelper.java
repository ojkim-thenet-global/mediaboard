package com.onk365.hahaha.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.onk365.hahaha.models.Adver;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by ravi on 15/03/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 11;

    // Database Name
    private static final String DATABASE_NAME = "advers_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(Adver.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Adver.TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public long insertAdver(int category_no, int slide_no,int slide_layout, int show_time, String thumb_src
            , String m0_type, String m0_src
            , String m1_type, String m1_src
            , String m2_type, String m2_src
            , String m3_type, String m3_src
            , String m4_type, String m4_src
            , String m5_type, String m5_src
            , String m6_type, String m6_src
            , String m7_type, String m7_src
            , String m8_type, String m8_src
            , String bg_type, String bg_src, String etc_val) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Adver.COLUMN_CATEGORY_NO, category_no);
        values.put(Adver.COLUMN_SLIDE_NO, slide_no);
        values.put(Adver.COLUMN_SLIDE_LAYOUT, slide_layout);
        values.put(Adver.COLUMN_SHOW_TIME, show_time);
        values.put(Adver.COLUMN_THUMB_SRC, thumb_src);
        values.put(Adver.COLUMN_M0_TYPE, m0_type);
        values.put(Adver.COLUMN_M0_SRC, m0_src);
        values.put(Adver.COLUMN_M1_TYPE, m1_type);
        values.put(Adver.COLUMN_M1_SRC, m1_src);
        values.put(Adver.COLUMN_M2_TYPE, m2_type);
        values.put(Adver.COLUMN_M2_SRC, m2_src);
        values.put(Adver.COLUMN_M3_TYPE, m3_type);
        values.put(Adver.COLUMN_M3_SRC, m3_src);
        values.put(Adver.COLUMN_M4_TYPE, m4_type);
        values.put(Adver.COLUMN_M4_SRC, m4_src);
        values.put(Adver.COLUMN_M5_TYPE, m5_type);
        values.put(Adver.COLUMN_M5_SRC, m5_src);
        values.put(Adver.COLUMN_M6_TYPE, m6_type);
        values.put(Adver.COLUMN_M6_SRC, m6_src);
        values.put(Adver.COLUMN_M7_TYPE, m7_type);
        values.put(Adver.COLUMN_M7_SRC, m7_src);
        values.put(Adver.COLUMN_M8_TYPE, m8_type);
        values.put(Adver.COLUMN_M8_SRC, m8_src);
        values.put(Adver.COLUMN_BG_TYPE, bg_type);
        values.put(Adver.COLUMN_BG_SRC, bg_src);
        values.put(Adver.COLUMN_ETC_VAL, etc_val);

        // insert row
        long id = db.insert(Adver.TABLE_NAME, null, values);

        // close db connection
        db.close();

        // return newly inserted row id
        return id;
    }

    public Adver getAdver(long category_id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Adver.TABLE_NAME,
                new String[]{Adver.COLUMN_ID, Adver.COLUMN_CATEGORY_NO, Adver.COLUMN_SLIDE_NO, Adver.COLUMN_SLIDE_LAYOUT, Adver.COLUMN_SHOW_TIME, Adver.COLUMN_THUMB_SRC,
                        Adver.COLUMN_M0_TYPE, Adver.COLUMN_M0_SRC,
                        Adver.COLUMN_M1_TYPE, Adver.COLUMN_M1_SRC,
                        Adver.COLUMN_M2_TYPE, Adver.COLUMN_M2_SRC,
                        Adver.COLUMN_M3_TYPE, Adver.COLUMN_M3_SRC,
                        Adver.COLUMN_M4_TYPE, Adver.COLUMN_M4_SRC,
                        Adver.COLUMN_M5_TYPE, Adver.COLUMN_M5_SRC,
                        Adver.COLUMN_M6_TYPE, Adver.COLUMN_M6_SRC,
                        Adver.COLUMN_M7_TYPE, Adver.COLUMN_M7_SRC,
                        Adver.COLUMN_M8_TYPE, Adver.COLUMN_M8_SRC,
                        Adver.COLUMN_BG_TYPE, Adver.COLUMN_BG_SRC,Adver.COLUMN_ETC_VAL},
                Adver.COLUMN_CATEGORY_NO + "=?",
                new String[]{String.valueOf(category_id)}, null, null, null, null);

        if (cursor != null)
            cursor.moveToFirst();

        // prepare adver object
        Adver adver = new Adver(
                cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_ID)),  // columnName으로 해당 레코드의 컬럼 위치(Index) 의 데이터 가져오기
                cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_CATEGORY_NO)),
                cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SLIDE_NO)),
                cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SLIDE_LAYOUT)),
                cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SHOW_TIME)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_THUMB_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M0_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M0_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M1_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M1_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M2_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M2_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M3_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M3_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M4_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M4_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M5_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M5_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M6_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M6_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M7_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M7_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M8_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M8_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_BG_TYPE)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_BG_SRC)),
                cursor.getString(cursor.getColumnIndex(Adver.COLUMN_ETC_VAL)));

        // close the db connection
        cursor.close();

        return adver;
    }

    public List<Adver> getAllAdvers(boolean weather) {
        List<Adver> advers = new ArrayList<>();
        String selectQuery;
            if(weather) {
                selectQuery = "SELECT  * FROM " + Adver.TABLE_NAME + " where " + Adver.COLUMN_CATEGORY_NO + " != 36";
            } else {
                selectQuery = "SELECT  * FROM " + Adver.TABLE_NAME + " where " + Adver.COLUMN_CATEGORY_NO + " not in (1,2,3,4,5,6,7,8,9, 36)";
            }

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Adver adver = new Adver();
                adver.setId(cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_ID)));
                adver.setCategoryNo(cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_CATEGORY_NO)));
                adver.setSlideNo(cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SLIDE_NO)));
                adver.setSlideLayout(cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SLIDE_LAYOUT)));
                adver.setShowTime(cursor.getInt(cursor.getColumnIndex(Adver.COLUMN_SHOW_TIME)));
                adver.setThumbSrc(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_THUMB_SRC)));
                adver.setM0Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M0_TYPE)));
                adver.setM0Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M0_SRC)));
                adver.setM1Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M1_TYPE)));
                adver.setM1Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M1_SRC)));
                adver.setM2Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M2_TYPE)));
                adver.setM2Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M2_SRC)));
                adver.setM3Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M3_TYPE)));
                adver.setM3Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M3_SRC)));
                adver.setM4Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M4_TYPE)));
                adver.setM4Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M4_SRC)));
                adver.setM5Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M5_TYPE)));
                adver.setM5Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M5_SRC)));
                adver.setM6Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M6_TYPE)));
                adver.setM6Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M6_SRC)));
                adver.setM7Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M7_TYPE)));
                adver.setM7Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M7_SRC)));
                adver.setM8Type(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M8_TYPE)));
                adver.setM8Src(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_M8_SRC)));
                adver.setBGType(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_BG_TYPE)));
                adver.setBGSrc(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_BG_SRC)));
                // 미디어보드 고도화 etc_val 추가
                adver.setEtcVal(cursor.getString(cursor.getColumnIndex(Adver.COLUMN_ETC_VAL)));
                /////////////////////////
                advers.add(adver);
            } while (cursor.moveToNext());
        }
        db.close();
        return advers;
    }

    public int getAdversCount() {
        String countQuery = "SELECT  * FROM " + Adver.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
    /*
    public int updateNote(Adver adver) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Adver.COLUMN_SEQ, adver.getSeq());
        values.put(Adver.COLUMN_TYPE, adver.getType());

        // updating row
        return db.update(Adver.TABLE_NAME, values, Adver.COLUMN_ID + " = ?",
                new String[]{String.valueOf(adver.getId())});
    }
    */
    public void deleteAdver(Adver adver) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Adver.TABLE_NAME, Adver.COLUMN_ID + " = ?",
                new String[]{String.valueOf(adver.getId())});
        db.close();
    }
    public void deleteAllAdver() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Adver.TABLE_NAME, Adver.COLUMN_ID + " > ?",
                new String[]{"0"});
        db.close();
    }
}
