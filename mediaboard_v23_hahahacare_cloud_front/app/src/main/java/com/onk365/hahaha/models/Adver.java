package com.onk365.hahaha.models;

/**
 * Created by ravi on 20/02/18.
 */

public class Adver {
    public static final String TABLE_NAME = "advers";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CATEGORY_NO = "category_no";
    public static final String COLUMN_SLIDE_NO = "slide_no";
    public static final String COLUMN_SLIDE_LAYOUT = "slide_layout";
    public static final String COLUMN_SHOW_TIME = "show_time";
    public static final String COLUMN_THUMB_SRC = "thumb_src";
    public static final String COLUMN_M0_TYPE = "m0_type";
    public static final String COLUMN_M0_SRC  = "m0_src";
    public static final String COLUMN_M1_TYPE = "m1_type";
    public static final String COLUMN_M1_SRC  = "m1_src";
    public static final String COLUMN_M2_TYPE = "m2_type";
    public static final String COLUMN_M2_SRC  = "m2_src";
    public static final String COLUMN_M3_TYPE = "m3_type";
    public static final String COLUMN_M3_SRC  = "m3_src";
    public static final String COLUMN_M4_TYPE = "m4_type";
    public static final String COLUMN_M4_SRC  = "m4_src";
    public static final String COLUMN_M5_TYPE = "m5_type";
    public static final String COLUMN_M5_SRC  = "m5_src";
    public static final String COLUMN_M6_TYPE = "m6_type";
    public static final String COLUMN_M6_SRC  = "m6_src";
    public static final String COLUMN_M7_TYPE = "m7_type";
    public static final String COLUMN_M7_SRC  = "m7_src";
    public static final String COLUMN_M8_TYPE = "m8_type";
    public static final String COLUMN_M8_SRC  = "m8_src";
    public static final String COLUMN_BG_TYPE = "bg_type";
    public static final String COLUMN_BG_SRC  = "bg_src";
    public static final String COLUMN_ETC_VAL = "etc_val";

    private int id;
    private int category_no;
    private int slide_no;
    private int slide_layout;
    private int show_time;
    private String thumb_src;
    private String m0_type;
    private String m0_src;
    private String m1_type;
    private String m1_src;
    private String m2_type;
    private String m2_src;
    private String m3_type;
    private String m3_src;
    private String m4_type;
    private String m4_src;
    private String m5_type;
    private String m5_src;
    private String m6_type;
    private String m6_src;
    private String m7_type;
    private String m7_src;
    private String m8_type;
    private String m8_src;
    private String bg_type;
    private String bg_src;
    private String etc_val;

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_CATEGORY_NO + " INT,"
                    + COLUMN_SLIDE_NO + " INT,"
                    + COLUMN_SLIDE_LAYOUT + "  INT,"
                    + COLUMN_SHOW_TIME + " INT,"
                    + COLUMN_THUMB_SRC + " VARCHAR(100),"
                    + COLUMN_M0_TYPE + " VARCHAR(100),"
                    + COLUMN_M0_SRC + " VARCHAR(100),"
                    + COLUMN_M1_TYPE + " VARCHAR(100),"
                    + COLUMN_M1_SRC + " VARCHAR(100),"
                    + COLUMN_M2_TYPE + " VARCHAR(100),"
                    + COLUMN_M2_SRC + " VARCHAR(100),"
                    + COLUMN_M3_TYPE + " VARCHAR(100),"
                    + COLUMN_M3_SRC + " VARCHAR(100),"
                    + COLUMN_M4_TYPE + " VARCHAR(100),"
                    + COLUMN_M4_SRC + " VARCHAR(100),"
                    + COLUMN_M5_TYPE + " VARCHAR(100),"
                    + COLUMN_M5_SRC + " VARCHAR(100),"
                    + COLUMN_M6_TYPE + " VARCHAR(100),"
                    + COLUMN_M6_SRC + " VARCHAR(100),"
                    + COLUMN_M7_TYPE + " VARCHAR(100),"
                    + COLUMN_M7_SRC + " VARCHAR(100),"
                    + COLUMN_M8_TYPE + " VARCHAR(100),"
                    + COLUMN_M8_SRC + " VARCHAR(100),"
                    + COLUMN_BG_TYPE + " VARCHAR(100),"
                    + COLUMN_BG_SRC + " VARCHAR(100),"
                    + COLUMN_ETC_VAL + " VARCHAR(100)"
                    + ")";

    public Adver() {
    }

    public Adver(int id, int category_no, int slide_no, int slide_layout, int show_time, String thumb_src
            , String m0_type, String m0_src
            , String m1_type, String m1_src
            , String m2_type, String m2_src
            , String m3_type, String m3_src
            , String m4_type, String m4_src
            , String m5_type, String m5_src
            , String m6_type, String m6_src
            , String m7_type, String m7_src
            , String m8_type, String m8_src
            , String bg_type, String bg_src, String etc_val
        ) {
        this.id = id;
        this.slide_no = category_no;
        this.slide_no = slide_no;
        this.slide_layout = slide_layout;
        this.show_time = show_time;
        this.thumb_src = thumb_src;
        this.m0_type = m0_type;
        this.m0_src  = m0_src;
        this.m1_type = m1_type;
        this.m1_src  = m1_src;
        this.m2_type = m2_type;
        this.m2_src  = m2_src;
        this.m3_type = m3_type;
        this.m3_src  = m3_src;
        this.m4_type = m4_type;
        this.m4_src  = m4_src;
        this.m5_type = m5_type;
        this.m5_src  = m5_src;
        this.m6_type = m6_type;
        this.m6_src  = m6_src;
        this.m7_type = m7_type;
        this.m7_src  = m7_src;
        this.m8_type = m8_type;
        this.m8_src  = m8_src;
        this.bg_type = bg_type;
        this.bg_src  = bg_src;
        this.etc_val  = etc_val;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getCategoryNo() {
        return category_no;
    }
    public void setCategoryNo(int category_no) {
        this.category_no = category_no;
    }
    public int getSlideNo() {
        return slide_no;
    }
    public void setSlideNo(int slide_no) {
        this.slide_no = slide_no;
    }

    public int getSlideLayout() {
        return slide_layout;
    }
    public void setSlideLayout(int slide_layout) {
        this.slide_no = slide_layout;
    }
    public int getShowTime() {
        return show_time;
    }
    public void setShowTime(int show_time) {
        this.show_time = show_time;
    }

    public String getThumbSrc() {return thumb_src;}
    public void setThumbSrc(String thumb_src) {
        this.thumb_src = thumb_src;
    }
    public String getM0Type() {
        return m0_type;
    }
    public void setM0Type(String m0_type) {
        this.m0_type = m0_type;
    }
    public String getM0Src() {
        return m0_src;
    }
    public void setM0Src(String m0_src) {
        this.m0_src = m0_src;
    }

    public String getM1Type() {
        return m1_type;
    }
    public void setM1Type(String m1_type) {
        this.m1_type = m1_type;
    }
    public String getM1Src() {
        return m1_src;
    }
    public void setM1Src(String m1_src) {
        this.m1_src = m1_src;
    }

    public String getM2Type() {
        return m2_type;
    }
    public void setM2Type(String m2_type) {
        this.m2_type = m2_type;
    }
    public String getM2Src() {
        return m2_src;
    }
    public void setM2Src(String m2_src) {
        this.m2_src = m2_src;
    }

    public String getM3Type() {
        return m3_type;
    }
    public void setM3Type(String m3_type) {
        this.m3_type = m3_type;
    }
    public String getM3Src() {
        return m3_src;
    }
    public void setM3Src(String m3_src) {
        this.m3_src = m3_src;
    }

    public String getM4Type() {
        return m4_type;
    }
    public void setM4Type(String m4_type) {
        this.m4_type = m4_type;
    }
    public String getM4Src() {
        return m4_src;
    }
    public void setM4Src(String m4_src) {
        this.m4_src = m4_src;
    }

    public String getM5Type() {
        return m5_type;
    }
    public void setM5Type(String m5_type) {
        this.m5_type = m5_type;
    }
    public String getM5Src() {
        return m5_src;
    }
    public void setM5Src(String m5_src) {
        this.m5_src = m5_src;
    }

    public String getM6Type() {
        return m6_type;
    }
    public void setM6Type(String m6_type) {
        this.m6_type = m6_type;
    }
    public String getM6Src() {
        return m6_src;
    }
    public void setM6Src(String m6_src) {
        this.m6_src = m6_src;
    }

    public String getM7Type() {
        return m7_type;
    }
    public void setM7Type(String m7_type) {
        this.m7_type = m7_type;
    }
    public String getM7Src() {
        return m7_src;
    }
    public void setM7Src(String m7_src) {
        this.m7_src = m7_src;
    }

    public String getM8Type() {
        return m8_type;
    }
    public void setM8Type(String m8_type) {
        this.m8_type = m8_type;
    }
    public String getM8Src() {
        return m8_src;
    }
    public void setM8Src(String m8_src) {
        this.m8_src = m8_src;
    }

    public String getBGType() {
        return bg_type;
    }
    public void setBGType(String bg_type) {
        this.bg_type = bg_type;
    }
    public String getBGSrc() {
        return bg_src;
    }
    public void setBGSrc(String bg_src) {
        this.bg_src = bg_src;
    }

    public String getEtcVal() {
        return etc_val;
    }
    public void setEtcVal(String etc_val) {
        this.etc_val = etc_val;
    }


}
