package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdversResponse {
    @SerializedName("code")
    private String code;
    @SerializedName("slideCount")
    private int slideCount;
    @SerializedName("slideList")
    private List<Adver> slideList;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public int getSlideCount() {
        return slideCount;
    }
    public void setSlideCount(int slideCount) {
        this.slideCount = slideCount;
    }
    public List<Adver> getSlideList() {
        return slideList;
    }
    public void setSlideList(List<Adver> slideList) {
        this.slideList = slideList;
    }

    public int getTotalResults() {
        return totalResults;
    }
    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
    public int getTotalPages() {
        return totalPages;
    }
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}