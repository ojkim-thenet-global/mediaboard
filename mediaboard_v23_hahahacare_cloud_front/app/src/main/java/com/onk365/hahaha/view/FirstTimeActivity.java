package com.onk365.hahaha.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.gson.Gson;
import com.onk365.hahaha.R;
import com.onk365.hahaha.client.ApiClient;
import com.onk365.hahaha.client.ApiClientTest;
import com.onk365.hahaha.client.ApiService;
import com.onk365.hahaha.managers.PrefManager;
import com.onk365.hahaha.models.LoginResponse;
import com.onk365.hahaha.models.SlideInfo;
import com.onk365.hahaha.models.StoreInfo;
import com.onk365.hahaha.models.WeathersResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by qwave on 2019-04-19
 * display_type A or B
 * store_no -> store_id
 * test completed
 * pm25Value add
 * btnList margin add "144dp"
 * layout_type = 21 Video Link URL -> HTML Tag
 * layout_type = 21 Video Link URL -> url == youtube? video view : webview
 * activity_first_time.xml UI
 * weather top text font size
 * survey webview close button
 * video layout tag = 20 error complete -> v13
 * API Master Info param -> store_id
 * BunchActivity, activity_bunch.xml UI update
 */

public class FirstTimeActivity extends Activity {
    private static final String TAG = FirstTimeActivity.class.getSimpleName();
    // TODO - insert your themoviedb.org API KEY here
    private final static String API_KEY = "11111";

    PrefManager prefMan;

    EditText mLoginid;
    EditText mLoginpw;
    ImageButton mSignIn;
    //private CheckBox mCheck;
    //Spinner mSpinner;

    ViewSwitcher mViewSwitcher;

    Context context;

    // 편성표 라디오버튼 추가
    RadioGroup radio_group;
    RadioButton type_a;
    RadioButton type_b;
    private String type;


    private List<WeathersResponse> weathersList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 화면을 portrait(세로) 화면으로 고정
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_first_time3);
        //setContentView(R.layout.activity_first_time);

        context = this;
        prefMan = new PrefManager(this);

        mLoginid = (EditText) findViewById(R.id.login_id);
        //mCheck = (CheckBox) findViewById(R.id.chkSave);
        //mJoin = (Button) findViewById(R.id.btnJoin);
        //if(prefMan.isIdSave()) {
        //    mLoginid.setText(prefMan.getStoreNo());
        //mCheck.setChecked(true);
        //}
        mSignIn = (ImageButton) findViewById(R.id.btnLogin);
        mViewSwitcher = (ViewSwitcher) findViewById(R.id.firt_time_view_switcher);
        mViewSwitcher.setInAnimation(context, R.anim.abc_fade_in);
        mViewSwitcher.setOutAnimation(context, R.anim.abc_fade_out);
        //mSpinner = (Spinner) findViewById(R.id.server_type);
        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.server_array,
                        android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        //mSpinner.setAdapter(staticAdapter);




        radio_group = (RadioGroup) findViewById(R.id.radio_group);
        type_a = (RadioButton) findViewById(R.id.type_a);
        type_b = (RadioButton) findViewById(R.id.type_b);


        type_a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "A";
                type_b.setChecked(false);
                type_a.setChecked(true);
                //Toast.makeText(context, "라디오 A : " + type, Toast.LENGTH_SHORT).show();
            }
        });

        type_b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type = "B";
                type_a.setChecked(false);
                type_b.setChecked(true);
                //Toast.makeText(context, "라디오 B : " + type, Toast.LENGTH_SHORT).show();
            }
        });


        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(mLoginid.getText().toString())) {
                    Toast.makeText(context, "상점코드를 입력하세요.", Toast.LENGTH_SHORT).show();

                }else if( type_a.isChecked() == false && type_b.isChecked() == false ) {
                    Toast.makeText(context, "편성표 타입을 선택하세요.", Toast.LENGTH_SHORT).show();

                }else {
                    //ID 자동저장
                    prefMan.setStoreNo(mLoginid.getText().toString());
                    prefMan.setIdSave(true);


                    // 편성표 타입
                    if(type_a.isChecked() == true) {

                        prefMan.setDisplayType("A");

                    }else if(type_b.isChecked() == true){

                        prefMan.setDisplayType("B");

                    }
                    //Toast.makeText(context, "슬라이드 타입 : " + prefMan.getDisplayType() , Toast.LENGTH_SHORT).show();


                    //Language
                    String loginid =mLoginid.getText().toString();

                    ApiService apiService;
                    if(loginid.indexOf("t")== 0) {
                        prefMan.setServerType("T");
                        apiService = ApiClientTest.getClient().create(ApiService.class);

                        loginid = loginid.substring(1) ;
                    } else {
                        prefMan.setServerType("R");
                        apiService = ApiClient.getClient().create(ApiService.class);
                    }

                    //String uuid = GetDevicesUUID(FirstTimeActivity.this);
                    if (mViewSwitcher.getDisplayedChild() == 0) {
                        mViewSwitcher.showNext();
                    }


                    Log.v(TAG, loginid);
                    Call<LoginResponse> call = apiService.getLogin(loginid);
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            int statusCode = response.code();
                            if (statusCode == 200) {
                                String code = response.body().getCode();
                                if (code.equals("100")) {

                                    StoreInfo store = response.body().getStoreInfo();

                                    String store_id = String.valueOf(store.getStoreId());
                                    int store_seq = store.getStoreSeq();
                                    //String store_no  = store.getStoreNo();

                                    // Log.v(TAG, store_no.toString());
                                    Log.d(TAG, "100 Number of user received: " + store_id);

                                    prefMan.setStoreId(store_id);
                                    prefMan.setStoreNo(String.valueOf(store_id));


                                    String display_type = prefMan.getDisplayType();
                                    onStoreInfo(store_id, display_type);

                                } else {
                                    fail("아이디가 없습니다.");
                                }
                            } else {
                                Log.d(TAG, "Error Number of user ERROR: " + statusCode);
                                fail("failed");
                            }
                        }
                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            // Log error here since request failed
                            Log.e(TAG, t.toString());
                            fail("failed");
                        }
                    });
                }
            }
        });




    }
    //약국정보조회
    private void onStoreInfo(String store_id, String display_type) {
        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<LoginResponse> call = apiService.getStore(store_id, display_type);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    String code = response.body().getCode();
                    if (code.equals("100")) {
                        StoreInfo store = response.body().getStoreInfo();
                        SlideInfo slide = response.body().getSlideInfo();
/*
    String nx = "59";
    String ny = "127";
    String address_pref = "서울";
    String address_county = "마포구";
    String areaNo = "1144000000";
 */
                        int x = store.getX();
                        int y = store.getY();
                        String store_name  = store.getStoreName();
                        String address_pref  = store.getAddressPref();
                        String address_county  = store.getAddressCounty();
                        String no  = store.getNo();
                        prefMan.setNX(x);
                        prefMan.setNY(y);
                        prefMan.setAddressPref(address_pref);
                        prefMan.setAddressCounty(address_county);
                        prefMan.setAreaNo(no);
                        prefMan.setStoreName(store_name);


                        // store_seq (구 store_id)
                        //String store_id = store.getStoreId();
                        //prefMan.setStoreId(store_id);
                        int store_seq = store.getStoreSeq();
                        prefMan.setStoreSeq(store_seq);

                        Log.d(TAG, "##############Number of user received: " + address_pref);
                        Log.d(TAG, "##############Number of user received: " + address_county);
                        Log.d(TAG, "##############Number of user received: " + no);




                        // display_code
                        String display_code = slide.getDisplayCode();
                        prefMan.setDisplayCode(display_code);
                        //Toast.makeText(context, "슬라이드 코드 : " + prefMan.getDisplayCode() , Toast.LENGTH_SHORT).show();

                        //signin();
                        fnStoreCheck(store_name);

                    } else {

                    }
                } else {
                    Log.d(TAG, "##############Number of user ERROR: " + statusCode);
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
    private void signin() {

        prefMan.setFirstLaunch(false);
        startActivity(new Intent(context, CheckActivity.class));
        finish();
    }
    private void fail(String result) {

        if (mViewSwitcher.getDisplayedChild() == 1) {
            mViewSwitcher.showPrevious();
        }
        Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
    }
    private void fnStoreCheck(String store_name) {
        new AlertDialog.Builder(FirstTimeActivity.this)
                .setTitle("약국명 확인!")
                .setMessage("[ " + store_name + " ] 으로 로그인 하시겠습니까?")
                .setCancelable(false)
                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        signin();
                    }
                })
                .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (mViewSwitcher.getDisplayedChild() == 1) {
                            mViewSwitcher.showPrevious();
                        }
                    }
                }).show();
    }
}