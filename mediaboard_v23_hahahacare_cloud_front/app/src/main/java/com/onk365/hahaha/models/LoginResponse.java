package com.onk365.hahaha.models;

/**
 * Created by ccy on 2016-08-16.
 */
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {
    @SerializedName("code")
    private String code;

    @SerializedName("storeInfo")
    private StoreInfo storeInfo;

    @SerializedName("slideInfo")
    private SlideInfo slideInfo;

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    public StoreInfo getStoreInfo() { return storeInfo; }
    public void setStoreInfo(StoreInfo storeInfo) {   this.storeInfo = storeInfo;}

    public SlideInfo getSlideInfo() { return slideInfo; }
    public void setSlideInfo(SlideInfo slideInfo) {   this.slideInfo = slideInfo;}

}