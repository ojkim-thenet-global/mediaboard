package com.onk365.hahaha.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.onk365.hahaha.R;
import com.onk365.hahaha.base.BaseActivity;

/**
 * Created by ccy on 2018-04-16.
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(SplashActivity.this,
                        CheckActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
