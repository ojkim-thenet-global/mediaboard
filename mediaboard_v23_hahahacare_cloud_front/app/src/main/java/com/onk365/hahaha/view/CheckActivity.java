package com.onk365.hahaha.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.onk365.hahaha.BuildConfig;
import com.onk365.hahaha.R;
import com.onk365.hahaha.base.BaseActivity;
import com.onk365.hahaha.client.ApiClient;
import com.onk365.hahaha.client.ApiClientTest;
import com.onk365.hahaha.client.ApiService;
import com.onk365.hahaha.database.DatabaseHelper;
import com.onk365.hahaha.managers.PrefManager;
import com.onk365.hahaha.models.Login;
import com.onk365.hahaha.models.LoginResponse;
import com.onk365.hahaha.models.SlideInfo;
import com.onk365.hahaha.models.StoreInfo;
import com.onk365.hahaha.models.UpdateResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ccy on 2018-04-16.
 */

public class CheckActivity extends BaseActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private DatabaseHelper db;

    PrefManager prefMan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        prefMan = new PrefManager(this);
        db = new DatabaseHelper(this);

        // 첫 로그인
        if (prefMan.isFirstLaunch()) {

            //로그인 체크
            startActivity(new Intent(this, FirstTimeActivity.class));
            finish();

        } else {
            //버전체크
            //onVersionCheck();
            //최초 설치(광고가 없을경우)
            //int count = db.getAdversCount() ;
            //Log.d(TAG, "count: " + count);
            /*
            if(count > 0) {
                //업데이트 체크
                onUpdateCheck(prefMan.getStoreId(), "2018-04-20 13:24:55");
            } else {
                startActivity(new Intent(this, BunchActivity.class));
                finish();
            }*/


            String datetime = getDateTime();
            prefMan.setDateTime(datetime);

            String store_id = prefMan.getStoreId();
            String display_code = prefMan.getDisplayCode();
            String display_type = prefMan.getDisplayType();


            if( (store_id == "") || (store_id == null) || (display_code == null) || (display_code == "") || (display_type == null) || (display_type == "") ) {

                //로그인 체크
                startActivity(new Intent(this, FirstTimeActivity.class));
                finish();
            }else{

                // 약국정보 업데이트
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if(networkInfo != null && networkInfo.isConnected()) {
                    if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                        //WIFI에 연결됨
                        onStoreInfo(store_id, display_type);
                    } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                        //LTE(이동통신망)에 연결됨
                        onStoreInfo(store_id, display_type);
                    }
                    Log.d("tj", "네트워크 정상");
                } else {
                    // 연결되지않음
                    Log.d("tj", "네트워크 오류!");
                    startActivity(new Intent(CheckActivity.this, BunchActivity.class));
                    finish();
                }

            }

        }
    }

    public NetworkInfo getNetworkState() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    public String getDateTime() {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        String datetime = format.format(date);

        return datetime;
    }



    // 약국정보조회 (display_code 업데이트)
    private void onStoreInfo(String store_id, String display_type) {


        ApiService apiService;
        if(prefMan.getServerType().equals("T")) {
            apiService = ApiClientTest.getClient().create(ApiService.class);
        } else {
            apiService = ApiClient.getClient().create(ApiService.class);
        }
        Call<LoginResponse> call = apiService.getStore(store_id, display_type);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                int statusCode = response.code();

                if (statusCode == 200) {
                    String code = response.body().getCode();
                    if (code.equals("100")) {
                        StoreInfo store = response.body().getStoreInfo();
                        SlideInfo slide = response.body().getSlideInfo();

                        int x = store.getX();
                        int y = store.getY();
                        String store_name  = store.getStoreName();
                        String address_pref  = store.getAddressPref();
                        String address_county  = store.getAddressCounty();
                        String no  = store.getNo();
                        prefMan.setNX(x);
                        prefMan.setNY(y);
                        prefMan.setAddressPref(address_pref);
                        prefMan.setAddressCounty(address_county);
                        prefMan.setAreaNo(no);
                        prefMan.setStoreName(store_name);

                        //int store_id = store.getStoreId();
                        //prefMan.setStoreId(store_id);
                        int seq = store.getStoreSeq();
                        prefMan.setStoreSeq(seq);

                        String display_code = slide.getDisplayCode();
                        prefMan.setDisplayCode(display_code);

                        //Toast.makeText(CheckActivity.this, "CheckActivity 슬라이드 코드 : " + prefMan.getDisplayCode() , Toast.LENGTH_SHORT).show();

                    } else {

                    }
                } else {
                    Log.d(TAG, "##############Number of user ERROR: " + statusCode);
                }


                String display_code = prefMan.getDisplayCode();

                if(display_code == "" || display_code == null) {

                    // 통신, code == 100이 아닐 때 전에 사용한 display_code가 없으면 페이지 이동 안함
                    startActivity(new Intent(CheckActivity.this, FirstTimeActivity.class));
                    finish();


                }else {
                    // 실패하면, 전에 있던 display_code가 그대로 사용됨
                    startActivity(new Intent(CheckActivity.this, BunchActivity.class));
                    finish();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }






    private void onUpdateCheck() {

        // 에러 안나게만 처리
        String store_id = prefMan.getStoreId();
        String datetime = "";
        String display_code = prefMan.getDisplayCode();
        String display_type = prefMan.getDisplayType();

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<UpdateResponse> call = apiService.getCheckSlide(store_id, datetime, display_type, display_code);

        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    String code = response.body().getCode();
                    if (code.equals("100")) {

                        String refresh = response.body().getSlideRefresh();

                        Log.d(TAG, "##############Number of user received: " + refresh);
                        //fnUpdateCheck(refresh);
                    } else {
                        Intent intent = new Intent(CheckActivity.this,
                                MainActivity.class);
                    }
                } else {
                    Log.d(TAG, "##############Number of user ERROR: " + statusCode);
                }
            }
            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    private void fnUpdateCheck(String slideRefresh) {
        Intent intent;
        if(slideRefresh.equals("N")) {
            intent = new Intent(CheckActivity.this,
                    MainActivity.class);
        } else {
            intent = new Intent(CheckActivity.this,
                    BunchActivity.class);
        }

        startActivity(intent);
        finish();
    }

    private void onVersionCheck() {

        ApiService apiService = ApiClient.getClient().create(ApiService.class);
        Call<Login> call = apiService.getVersion();

        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    String msg = response.body().getCode();
                    Log.d(TAG, "##############Number of user received: " + msg);
                    fnVersionCheck(msg);
                } else {
                    Log.d(TAG, "##############Number of user ERROR: " + statusCode);
                }
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });

    }
    private void fnVersionCheck(String LastVersion) {
        String currentVersion = getVersionName(getBaseContext()) ;
        if(! currentVersion.equals(LastVersion)) {
            new AlertDialog.Builder(CheckActivity.this)
                    .setTitle("업데이트 필요!")
                    .setMessage("프로그램을 최신 버전으로 업데이트 해주셔야 사용 가능합니다!")
                    .setCancelable(false)
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                    CheckActivity.this.finish();
                                }
                            });
                        }
                    })
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(CheckActivity.this, "프로그램을 종료합니다!", Toast.LENGTH_SHORT).show();
                                    CheckActivity.this.finish();
                                }
                            });
                        }
                    })
                    .show();
        }
    }
    private static String getVersionName(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

            return null;
        }
    }
}
