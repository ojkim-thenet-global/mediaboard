package com.onk365.hahaha.client;

import com.onk365.hahaha.models.AdversResponse;
import com.onk365.hahaha.models.Login;
import com.onk365.hahaha.models.LoginResponse;
import com.onk365.hahaha.models.MasterResponse;
import com.onk365.hahaha.models.UpdateResponse;
import com.onk365.hahaha.models.WeathersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

//import com.onk365.hahaha.models.Adver;
//import retrofit2.http.GET;

//import retrofit2.http.Path;
public interface ApiService {


    //버전체크 플레이스토어
    @POST("/service/get-version")
    Call<Login> getVersion();

    //인증(로그인)
    @FormUrlEncoded
    @POST("auth_storeLogin.kis")
    //Call<LoginResponse> getLogin(@Field("store_no") String store_no);
    Call<LoginResponse> getLogin(@Field("store_id") String store_id);

    //약국정보
    @FormUrlEncoded
    @POST("store_getStore.kis")
    //Call<LoginResponse> getStore(@Field("store_id") String store_no);
    Call<LoginResponse> getStore(@Field("store_id") String store_id, @Field("display_type") String display_type);

    //마스터정보
    @FormUrlEncoded
    @POST("master_getMaster.kis")
    Call<MasterResponse> getMastersList(@Field("store_id") String store_id);
    /*
    //마스터정보
    @POST("master_getMaster.kis")
    Call<MasterResponse> getMastersList();
    */

    //날씨정보
    @FormUrlEncoded
    @POST("weather_getWeatherInfo.kis")
    Call<WeathersResponse> getWeatherList(@Field("nx") int nx, @Field("ny") int ny, @Field("address_pref") String address_pref, @Field("address_county") String address_county, @Field("areaNo") String areaNo, @Field("systemCharacter") String systemCharacter);
    //Call<WeathersResponse> getWeatherList(@Field("nx") int nx, @Field("ny") int ny, @Field("address_pref") String address_pref, @Field("address_county") String address_county, @Field("areaNo") String areaNo);


    //편성표별 컨텐츠 정보 (슬라이드정보)
    @FormUrlEncoded
    @POST("slide_getDisplaySlide.kis")
    //Call<AdversResponse> getSlidesList(@Field("store_no") String store_no, @Field("display_type") String display_type);
    Call<AdversResponse> getSlidesList(@Field("store_id") String store_id, @Field("display_type") String display_type);

    //편성표 버전정보 (슬라이드 업데이트)
    @FormUrlEncoded
    @POST("slide_getRefreshCheck.kis")
    Call<UpdateResponse> getCheckSlide(@Field("store_id") String store_id, @Field("datetime") String datetime, @Field("display_type") String display_type, @Field("display_code") String display_code);
    //Call<UpdateResponse> getCheckSlide();

}