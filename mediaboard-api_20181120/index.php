<?php
require_once __DIR__ . '/app/app.php';

//$common->enableCORS();

header("Content-type: application/json; charset=utf-8");

$app = new Bullet\App();

$routesDir = SRC_ROOT . 'routes/';

require_once $routesDir . 'index.php';
require_once $routesDir . 'auth.php';
require_once $routesDir . 'store.php';
require_once $routesDir . 'slide.php';
require_once $routesDir . 'weather.php';
require_once $routesDir . 'masterdata.php';
require_once $routesDir . 'service.php';

$app->run(new Bullet\Request())->send();
