<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit279a1a1cedce15fb0e186f353d940ac4
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Psr\\Container\\' => 14,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Psr\\Container\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/container/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'P' => 
        array (
            'Pimple' => 
            array (
                0 => __DIR__ . '/..' . '/pimple/pimple/src',
            ),
        ),
        'B' => 
        array (
            'Bullet' => 
            array (
                0 => __DIR__ . '/..' . '/vlucas/bulletphp/src',
            ),
        ),
    );

    public static $classMap = array (
        'EasyPeasyICS' => __DIR__ . '/..' . '/phpmailer/phpmailer/extras/EasyPeasyICS.php',
        'PHPMailer' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmailer.php',
        'PHPMailerOAuth' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmaileroauth.php',
        'PHPMailerOAuthGoogle' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmaileroauthgoogle.php',
        'POP3' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.pop3.php',
        'SMTP' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.smtp.php',
        'ntlm_sasl_client_class' => __DIR__ . '/..' . '/phpmailer/phpmailer/extras/ntlm_sasl_client.php',
        'phpmailerException' => __DIR__ . '/..' . '/phpmailer/phpmailer/class.phpmailer.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit279a1a1cedce15fb0e186f353d940ac4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit279a1a1cedce15fb0e186f353d940ac4::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit279a1a1cedce15fb0e186f353d940ac4::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit279a1a1cedce15fb0e186f353d940ac4::$classMap;

        }, null, ClassLoader::class);
    }
}
