<?php
function autoload_module($class_name)
{
    $array_paths = array(
        SRC_ROOT . 'lib',
        SRC_ROOT . 'config',
        SRC_ROOT . 'db'
    );

    foreach ($array_paths as $path) {
        //$file = sprintf($_SERVER['DOCUMENT_ROOT'] . '/%s.php', str_replace("\\", "/", $class_name));
        $file = sprintf(SRC_ROOT . '/%s.php', str_replace("\\", "/", $class_name));

        if (is_file($file)) include_once $file;
    }
}

spl_autoload_register('autoload_module');
