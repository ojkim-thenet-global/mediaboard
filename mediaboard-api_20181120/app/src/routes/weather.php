<?php

use lib\Common as Common;
use lib\Weather as Weather;

$common = new Common();
$weather = new Weather();

$app->path('weather', function ($request) use ($app, $common, $weather) {

    $app->path('get-weather-info', function ($request) use ($app, $common, $weather) {

        $app->post(function ($request) use ($app, $common, $weather) {

            try {

                $checkParams = $common->checkRequestParams(array('nx', 'ny', 'address_pref', 'address_county', 'areaNo'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $nx = $request->post('nx');
				//$nx = strval($nx);
                $ny = $request->post('ny');
				//$ny = strval($ny);
				$address_pref = $request->post('address_pref');
                $address_county = $request->post('address_county');
                $areaNo = $request->post('areaNo');

                // 초단기 실황조회 - 현재 날씨, 현재 온도
                $weatherInfo = $weather->getWeather($nx, $ny);

                // 동네예보조회 - 최저온도
                $low = $weather->getLowTemperature($nx, $ny);

                // 동네예보조회 - 최고온도
                $high = $weather->getHighTemperature($nx, $ny);

                // 자외선지수 조회
                $uv = $weather->getUvRay($areaNo);

                // 체감온도 조회
                $wind = $weather->getWindChill($areaNo);

                // 식중독지수 조회
                $food = $weather->getFoodPoisoning($areaNo);

                // 동파가능지수 조회
                $frozen = $weather->getFrozen($areaNo);

                // 불쾌지수 조회
                $discomfort = $weather->getDiscomfort($areaNo);

                // 감기지수 조회
                $cold = $weather->getCold($areaNo);

                // 피부질환지수 조회
                $skin = $weather->getSkinDisease($areaNo);

                // 호흡기질환지수 조회
                $respiratory = $weather->getRespiratoryDisease($areaNo);

                // 뇌졸중가능지수 조회
                $stroke = $weather->getStroke($areaNo);

                // 미세먼지 조회
                $part = $weather->getParticulate($address_pref, $address_county);

                if ($weatherInfo == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'weather' => $weatherInfo,
                        'low' => $low,
                        'high' => $high,
                        "uv" => $uv,
                        'wind' => $wind,
                        'food' => $food,
                        'frozen' => $frozen,
                        'discomfort' => $discomfort,
                        'cold' => $cold,
                        'skin' => $skin,
                        'respiratory' => $respiratory,
                        'stroke' => $stroke,
                        'part' => $part
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
