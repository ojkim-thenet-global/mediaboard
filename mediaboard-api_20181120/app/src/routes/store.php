<?php

use lib\Common as Common;
use lib\Store as Store;

$common = new Common();
$store = new Store();

$app->path('store', function ($request) use ($app, $common, $store) {

    $app->path('get-store', function ($request) use ($app, $common, $store) {

        $app->post(function ($request) use ($app, $common, $store) {

            try {

                $checkParams = $common->checkRequestParams(array('store_id'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $store_id = $request->post('store_id');

                $result = $store->getStore($store_id);

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'storeInfo' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
