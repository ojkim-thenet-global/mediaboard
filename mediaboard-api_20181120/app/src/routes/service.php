<?php

use lib\Common as Common;
use lib\Service as Service;

$common = new Common();
$service = new Service();

$app->path('service', function ($request) use ($app, $common, $service) {

    $app->path('get-version', function ($request) use ($app, $common, $service) {

        $app->post(function ($request) use ($app, $common, $service) {

            try {

                $result = $service->getVersion();

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'version' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
