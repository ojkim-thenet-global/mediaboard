<?php

use lib\Common as Common;
use lib\Auth as Auth;

$common = new Common();
$auth = new Auth();

$app->path('auth', function ($request) use ($app, $common, $auth) {

    $app->path('admin-login', function ($request) use ($app, $common, $auth) {

        $app->post(function ($request) use ($app, $common, $auth) {

            try {

                $checkParams = $common->checkRequestParams(array('id', 'password'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $id = $request->post('id');
                $password = $request->post('password');

                $result = $auth->adminLogin($id, $password);

                if ($result == null) {
                    $response = $common->apiMsg('400');
                } else {
                    $data = array(
                        'adminInfo' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

    $app->path('store-login', function ($request) use ($app, $common, $auth) {

        $app->post(function ($request) use ($app, $common, $auth) {

            try {

                $checkParams = $common->checkRequestParams(array('store_no'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $store_no = $request->post('store_no');

                $result = $auth->storeLogin($store_no);

                if ($result == null) {
                    $response = $common->apiMsg('400');
                } else {
                    $data = array(
                        'storeInfo' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
