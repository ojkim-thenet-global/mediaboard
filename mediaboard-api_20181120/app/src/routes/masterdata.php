<?php

use lib\Common as Common;
use lib\MasterData as MasterData;

$common = new Common();
$masterData = new MasterData();

$app->path('masterdata', function ($request) use ($app, $common, $masterData) {

    $app->path('get-master-data', function ($request) use ($app, $common, $masterData) {

        $app->post(function ($request) use ($app, $common, $masterData) {

            try {

                $result = $masterData->getMasterData();

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'masterDataInfo' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
