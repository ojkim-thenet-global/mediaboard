<?php

use lib\Common as Common;
use lib\Slide as Slide;

$common = new Common();
$slide = new Slide();

$app->path('slide', function ($request) use ($app, $common, $slide) {

    $app->path('get-common-slide', function ($request) use ($app, $common, $slide) {

        $app->post(function ($request) use ($app, $common, $slide) {

            try {

                $result = $slide->getCommonSlide();

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'slideCount' => count($result),
                        'slideList' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

    $app->path('get-store-slide', function ($request) use ($app, $common, $slide) {

        $app->post(function ($request) use ($app, $common, $slide) {

            try {

                $checkParams = $common->checkRequestParams(array('store_id'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $store_id = $request->post('store_id');

                $result = $slide->getStoreSlide($store_id);

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'slideCount' => count($result),
                        'slideList' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

    $app->path('get-display-slide', function ($request) use ($app, $common, $slide) {

        $app->post(function ($request) use ($app, $common, $slide) {

            try {

                $checkParams = $common->checkRequestParams(array('store_id'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $store_id = $request->post('store_id');

                $result = $slide->getDisplaySlide($store_id);

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'slideCount' => count($result),
                        'slideList' => $result
                    );

                    $response = $common->apiMsg("100", null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

    $app->path('check-slide-refresh', function ($request) use ($app, $common, $slide) {

        $app->post(function ($request) use ($app, $common, $slide) {

            try {

                $checkParams = $common->checkRequestParams(array('store_id', 'datetime'));
                if (!$checkParams) {
                    $response = $common->apiMsg('110');
                    return $response;
                }

                $store_id = $request->post('store_id');
                $datetime = $request->post('datetime'); // yyyy-mm-dd hh:mm:ss 형식

                $result = $slide->checkSlideRefresh($store_id, $datetime);

                if ($result == null) {
                    $response = $common->apiMsg('120');
                } else {
                    $data = array(
                        'slideRefresh' => $result
                    );

                    $response = $common->apiMsg('100', null, $data);
                }

                return $response;

            } catch (Exception $e) {

                $response = $common->apiMsg('900', $e, null);

                return $response;

            }
        });

    });

});
