<?php

use lib\Common as Common;

$common = new Common();

$app->path('/', function ($request) use ($app, $common) {

    $common->log("Welcome!!");

    $data = array(
        'greeting' => 'Media Board API'
    );

    $response = $common->apiMsg("100", null, $data);

    return $response;

});
