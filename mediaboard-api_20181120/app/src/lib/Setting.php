<?php

namespace lib;

use config\Config as Config;

class Setting {

	private static $self;

	private $common;
	private $setting;

	public function __construct( $ini_file ) {
		$this->common = Common::getInstance();

		$this->setting = parse_ini_file( $ini_file, true );
		//$this->common->log("setting=", $this->setting);
	}

	public static function getInstance() {
		if ( ! self::$self ) {
			self::$self = new Setting( Config::get( 'ini_file' ) );
		}

		return self::$self;
	}

	public function __get( $setting ) {
		//$this->common->log("here... " . $setting);
		if ( array_key_exists( $setting, $this->setting ) ) {
			return $this->setting[ $setting ];
		} else {
			foreach ( $this->setting as $section ) {
				if ( array_key_exists( $setting, $section ) ) {
					return $section[ $setting ];
				}
			}
		}
	}


}
