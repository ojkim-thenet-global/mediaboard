<?php

namespace lib;

use config\Config as Config;
use db\MySQLDB as DB;

class Auth
{
    private static $self;

    private $db;
    private $common;

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->common = Common::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Auth();

        return self::$self;
    }

    public function adminLogin($id, $password)
    {
        $sql = "SELECT * FROM m_admin WHERE id = ? AND pw = ? ";

        $params = array($id, $password);

        $result = $this->db->rawQuery($sql, $params);

        if ($result == null) return false;

        return $result;
    }

    public function storeLogin($store_no)
    {
        $sql = "SELECT * FROM t_drugstore WHERE store_no = ? AND valid = 'Y' ";

        $params = array($store_no);

        $result = $this->db->rawQuery($sql, $params);

        if ($result == null) return false;

        $storeInfo = $result[0];

        return $storeInfo;
    }


}
