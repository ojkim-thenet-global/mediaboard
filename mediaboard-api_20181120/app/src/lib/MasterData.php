<?php

namespace lib;

use config\Config as Config;
use db\MySQLDB as DB;

class MasterData
{
    private static $self;

    private $db;
    private $common;

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->common = Common::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new MasterData();

        return self::$self;
    }

    // 마스터 데이터 획득
    public function getMasterData()
    {
        $upper = '';
        $lower = '';
        $survey = '';

        $sql = "SELECT * FROM m_data ";

        $result = $this->db->rawQuery($sql);

        if ($result == null) return false;

        for ($i = 0; $i <= count($result); $i++) {
            switch ($result[$i]['key']) {
                case 'upper':
                    $upper = $result[$i]['value'];
                    break;
                case 'lower':
                    $lower = $result[$i]['value'];
                    break;
                case 'survey':
                    $survey = $result[$i]['value'];
                    break;
                default:
            }
        }

        $response = array(
            'upper' => $upper,
            'lower' => $lower,
            'survey' => $survey
        );

        //$this->common->log('response=', $response);

        return $response;
    }


}
