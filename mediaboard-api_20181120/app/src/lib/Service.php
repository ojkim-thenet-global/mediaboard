<?php

namespace lib;

use config\Config as Config;
//use db\MySQLDB as DB;

class Service
{
    private static $self;

    //private $db;
    private $common;
	private $setting;

    public function __construct()
    {
        //$this->db = DB::getInstance();
        $this->common = Common::getInstance();
        $this->setting = Setting::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Service();

        return self::$self;
    }

    // App 버전 정보
    public function getVersion()
    {
		return $this->setting->version;
    }


}
