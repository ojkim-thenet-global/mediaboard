<?php

namespace lib;

use config\Config as Config;
use db\MySQLDB as DB;

class Slide
{
    private static $self;

    private $db;
    private $common;

    const TEXT = 30;
    const TEXT_TOP = 31;
    const TEXT_RIGHT = 32;
    const TEXT_BOTTOM = 33;
    const TEXT_LEFT = 34;

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->common = Common::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Slide();

        return self::$self;
    }

    public function checkSlideRefresh($store_id, $datetime)
    {
        $refresh = "N";

        // 날짜가 바뀐 경우, 그룹 표시 날짜 확인
        if (date("Y-m-d") != substr($datetime, 0, 10)) {
            if ($this->checkSlideGroupDate() > 0) {
                $refresh = "Y";
            }
        }

        // 슬라이드 갱신 내역 확인
        if ($this->checkSlideRefreshInfo($store_id, $datetime) > 0) {
            $refresh = "Y";
        }

        return $refresh;
    }

    // 날짜 변경으로 인한 슬라이드 갱신 유무 체크
    private function checkSlideGroupDate()
    {
        $sql = "SELECT COUNT(1) cnt FROM t_group WHERE ";
        $sql .= "group_show = 'Y' ";
        $sql .= "AND (group_avail_start = CURDATE() OR group_avail_end = SUBDATE(CURDATE(), 1)) ";
        //$this->common->log(__METHOD__, $sql);

        $result = $this->db->rawQuery($sql);

        return intval($result["cnt"]);
    }

    // 슬라이드 갱신 체크
    private function checkSlideRefreshInfo($store_id, $datetime)
    {
        //$this->common->log(__METHOD__ . " called");

        $sql = "SELECT COUNT(store_id) cnt FROM t_slide_refresh WHERE ";
        $sql .= "store_id IN (0 ";
        if ($store_id != "") $sql .= ", ? ";
        $sql .= " ) AND update_dt > ? ";
        //$this->common->log(__METHOD__, $sql);

        $params = array($store_id, $datetime);

        $result = $this->db->rawQuery($sql, $params);

        return intval($result["cnt"]);
    }

    public function getDisplaySlide($store_id)
    {
        //$this->common->log(__METHOD__ . " called");

        $commonSlideList = $this->getCommonSlide();
        //$this->common->log(__METHOD__." commonSlideList", $commonSlideList);
        //$this->common->log(__METHOD__." commonSlideList", count($commonSlideList));

        $storeSlideList = $this->getStoreSlide($store_id);
        //$this->common->log(__METHOD__." storeSlideList", $storeSlideList);
        //$this->common->log(__METHOD__." storeSlideList", count($storeSlideList));

        $slideList = array();

        // 두 슬라이드 목록을 하나로 합침
        $ci = 0;        // 공통 슬라이드 인덱스
        $si = 0;        // 약국 슬라이드 인덱스
        $groupCnt = 0;  // 그룹 카운터

        while ($ci < count($commonSlideList) || $si < count($storeSlideList)) {

            // 약국 슬라이드 순서 인덱스와 그룹 카운터를 비교하여 어느 쪽을 삽입할지 결정
            $checkStoreSlide = ($ci >= count($commonSlideList)) || ($si < count($storeSlideList) && $groupCnt >= $storeSlideList[$si]["order_index"]);

            $slideInfo = $checkStoreSlide ? $storeSlideList[$si] : $commonSlideList[$ci];

            $slideInfo["m0_src"] = $this->getMaterialSource($slideInfo["m0_type"], $slideInfo["m0_src"]);
            $slideInfo["m1_src"] = $this->getMaterialSource($slideInfo["m1_type"], $slideInfo["m1_src"]);
            $slideInfo["m2_src"] = $this->getMaterialSource($slideInfo["m2_type"], $slideInfo["m2_src"]);
            $slideInfo["m3_src"] = $this->getMaterialSource($slideInfo["m3_type"], $slideInfo["m3_src"]);
            $slideInfo["m4_src"] = $this->getMaterialSource($slideInfo["m4_type"], $slideInfo["m4_src"]);
            $slideInfo["m5_src"] = $this->getMaterialSource($slideInfo["m5_type"], $slideInfo["m5_src"]);
            $slideInfo["m6_src"] = $this->getMaterialSource($slideInfo["m6_type"], $slideInfo["m6_src"]);
            $slideInfo["m7_src"] = $this->getMaterialSource($slideInfo["m7_type"], $slideInfo["m7_src"]);
            $slideInfo["m8_src"] = $this->getMaterialSource($slideInfo["m8_type"], $slideInfo["m8_src"]);

            array_push($slideList, $slideInfo);

            if ($checkStoreSlide) {
                $si += 1;
            } else {
                $ci += 1;
                // 다음의 공통 슬라이드가 표시된 공통 슬라이드와 다른 그룹인 경우 카운트 추가
                if ($ci < count($commonSlideList) && (($commonSlideList[$ci]["category_no"] != $commonSlideList[$ci - 1]["category_no"]) ||
                        ($commonSlideList[$ci]["group_no"] != $commonSlideList[$ci - 1]["group_no"]))
                ) {
                    $groupCnt += 1;
                }
            }

        }

        return $slideList;
    }

    // 공통 슬라이드 조회
    public function getCommonSlide()
    {
        $sql = "SELECT s.category_no, s.group_no, s.slide_no, s.slide_layout, s.show_time, s.thumb_src, s.m0_type, s.m0_src ";
        $sql .= ", s.m1_type, s.m1_src, s.m2_type, s.m2_src, s.m3_type, s.m3_src, s.m4_type, s.m4_src, s.m5_type, s.m5_src ";
        $sql .= ", s.m6_type, s.m6_src, s.m7_type, s.m7_src, s.m8_type, s.m8_src, s.bg_type, s.bg_src, etc_val ";
        $sql .= "FROM t_slide s ";
        $sql .= "INNER JOIN t_group g ";
        $sql .= "ON s.category_no = g.category_no ";
        $sql .= "AND s.group_no = g.group_no ";
        $sql .= "INNER JOIN t_category c ";
        $sql .= "ON g.category_no = c.category_no ";
        $sql .= "WHERE g.group_show = 'Y' ";
        $sql .= "AND (g.group_avail_start is null or g.group_avail_start <= CURDATE()) ";
        $sql .= "AND (g.group_avail_end is null or g.group_avail_end >= CURDATE()) ";
        $sql .= "ORDER BY c.category_order ASC, ";
        $sql .= "g.group_order ASC, ";
        $sql .= "s.slide_order ASC ";
        //$this->common->log(__METHOD__, $sql);

        $slideList = $this->db->rawQuery($sql);

        return $slideList;
    }

    // 약국 슬라이드 조회
    public function getStoreSlide($store_id)
    {
    	// store 슬라이드의 category_no 를 기본값(10)으로 설정
        //$sql = "SELECT store_id, slide_no, slide_layout, order_index, show_time, thumb_src, m0_type, m0_src ";
        $sql = "SELECT 10 as category_no, store_id, slide_no, slide_layout, order_index, show_time, thumb_src, m0_type, m0_src ";
        $sql .= ", m1_type, m1_src, m2_type, m2_src, m3_type, m3_src, m4_type, m4_src, m5_type, m5_src ";
        $sql .= ", m6_type, m6_src, m7_type, m7_src, m8_type, m8_src, bg_type, bg_src, etc_val ";
        $sql .= "FROM t_store_slide ";
        $sql .= "WHERE order_index >= 0 ";
        $sql .= "AND store_id = ? ";
        $sql .= "ORDER BY order_index ASC ";
        //$this->common->log(__METHOD__, $sql);

        $params = array($store_id);

        $slideList = $this->db->rawQuery($sql, $params);

        return $slideList;
    }

    // 텍스트 리소스인지 판단하여, 시퀀스로부터 텍스트를 가져와 내용물을 반환
    private function getMaterialSource($m_type, $m_src)
    {
        //$this->common->log(__METHOD__ . " called: m_type=" . $m_type . "|m_src=" . $m_src);

        $returnVal = $m_src;

        if ($this->checkTypeText($m_type)) {
            $returnVal = "";

            $return = $this->getTextMaterial($m_src);
            //$this->common->log(__METHOD__, $return);

            $returnVal = $return["content"];
        }

        return $returnVal;
    }

    // 텍스트 리소스 선택
    private function getTextMaterial($text_seq)
    {
        $sql = "SELECT * FROM t_text_material WHERE text_seq = ? ";

        $params = array($text_seq);

        $result = $this->db->rawQuery($sql, $params);

        return $result;
    }

    // 리소스 타입이 텍스트형태인지 체크
    private function checkTypeText($m_type)
    {
        //$this->common->log(__METHOD__ . " called");

        //$m_type = intval($m_type);
        return ($m_type == self::TEXT || $m_type == self::TEXT_TOP || $m_type == self::TEXT_RIGHT || $m_type == self::TEXT_BOTTOM || $m_type == self::TEXT_LEFT);
    }


}
