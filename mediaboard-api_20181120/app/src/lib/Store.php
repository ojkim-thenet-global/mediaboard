<?php

namespace lib;

use config\Config as Config;
use db\MySQLDB as DB;

class Store
{
    private static $self;

    private $db;
    private $common;

    public function __construct()
    {
        $this->db = DB::getInstance();
        $this->common = Common::getInstance();
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Store();

        return self::$self;
    }

    // 약국 정보 조회
    public function getStore($store_id)
    {
        $sql = "SELECT * FROM t_drugstore WHERE store_id = ?";

        $params = array($store_id);

        $result = $this->db->rawQuery($sql, $params);
        if ($result == null) return false;

        $storeInfo = $result[0];

        if (array_key_exists('address_pref', $storeInfo)) {
            //$this->common->log('here....');
            $address_pref = $storeInfo["address_pref"];
            $address_county = array_key_exists('address_county', $storeInfo) ? $storeInfo["address_county"] : '';
            $address_town = array_key_exists('address_town', $storeInfo) ? $storeInfo["address_town"] : '';

            // 약국 주소로부터 기상 정보 API용 코드 획득
            $areaCode = $this->getStoreAreaCode($address_pref, $address_county, $address_town);

            $storeInfo = array_merge($storeInfo, $areaCode);
        }

        return $storeInfo;
    }

    // 약국 주소로부터 기상 정보 API용 코드 획득
    public function getStoreAreaCode($address_pref, $address_country, $address_town)
    {
        $areaCode = array();

        $weatherArea = $this->getWeatherArea($address_pref, $address_country, $address_town);
        //$this->common->log('getWeatherArea', $weatherArea);

        if($weatherArea) {
            $areaCode['x'] = $weatherArea['x'];
            $areaCode['y'] = $weatherArea['y'];
        }

        $lifeArea = $this->getLifeArea($address_pref, $address_country, $address_town);
        //$this->common->log('getLifeArea', $weatherArea);

        if($lifeArea) {
            $areaCode['no'] = $lifeArea['no'];
        }

        //$this->common->log('areaCode', $areaCode);

        return $areaCode;
    }

    // 날씨 API 코드 획득
    public function getWeatherArea($address_pref, $address_county, $address_town)
    {
        $sql  = "SELECT * FROM m_weather_area ";
        $sql .= "WHERE ";
        $sql .= "    address_pref = ? ";
        $sql .= "AND address_county in ('', ?) ";
        $sql .= "AND address_town in ('', ?) ";
        $sql .= "ORDER BY address_county DESC, address_town DESC ";
        $sql .= "LIMIT 1";

        $params = array($address_pref, $address_county, $address_town);

        $result = $this->db->rawQuery($sql, $params);

        return $result[0];
    }

    // 날씨 API 코드 획득
    public function getLifeArea($address_pref, $address_county, $address_town)
    {
        $sql  = "SELECT * FROM m_life_area ";
        $sql .= "WHERE address_pref = ? ";
        $sql .= "AND address_county in ('', ?) ";
        $sql .= "AND address_town in ('', ?) ";
        $sql .= "ORDER BY address_county DESC, address_town DESC ";
        $sql .= "LIMIT 1 ";

        $params = array($address_pref, $address_county, $address_town);

        $result = $this->db->rawQuery($sql, $params);

        return $result[0];
    }


}
