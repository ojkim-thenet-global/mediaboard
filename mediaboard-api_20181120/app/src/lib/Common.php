<?php

namespace lib;

use config\Config as Config;

class Common
{
    private static $self;

    function __construct()
    {
    }

    public static function getInstance()
    {
        if (!self::$self) self::$self = new Common();

        return self::$self;
    }

    public function log($sys, $mess = '')
    {
        $dir = Config::get('log_path');
        $this->makedirs($dir, '0755');

        $filename = 'app_' . date('Ymd') . '.log';
        $fp = fopen($dir . $filename, "a+");
        if ($fp == null)
            return;

        if (is_array($mess)) {
            // $mess = serialize($mess);
            $mess = $this->_contextToString($mess);
        }

        // fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . " " . $mess . "\n");
        fwrite($fp, date("Y-m-d H:i:s") . " " . $sys . "\n" . $mess . "\n");

        fclose($fp);
    }

    private function _contextToString($context)
    {
        $export = '';
        foreach ($context as $key => $value) {
            $export .= "{$key}: ";
            $export .= preg_replace(array(
                '/=>\s+([a-zA-Z])/im',
                '/array\(\s+\)/im',
                '/^  |\G  /m'
            ), array(
                '=> $1',
                'array()',
                '    '
            ), str_replace('array (', 'array(', var_export($value, true)));
            $export .= PHP_EOL;
        }

        return str_replace(array(
            '\\\\',
            '\\\''
        ), array(
            '\\',
            '\''
        ), rtrim($export));
    }

    public function makedirs($dirpath, $mode = 0777)
    {
        return is_dir($dirpath) || @mkdir($dirpath, $mode, true);
    }

    public function enableCORS($bool = false)
    {
        if ($bool) {
            /*
            header("Access-Control-Allow-Origin: *");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
            header("Access-Control-Allow-Headers: Content-Type,Depth, User-Agent,X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
            */

            if (isset($_SERVER['HTTP_ORIGIN'])) {
                header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
                header("Access-Control-Allow-Credentials: true");
                //header("Access-Control-Max-Age: 86400");    // cache for 1 day
            }

            if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                    header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

                exit(0);
            }

            return true;
        }
    }

    public function checkRequestParams($required_fields)
    {
        $request_params = $_REQUEST;

        $error = false;

        if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            $app = new \Bullet\App();
            parse_str($app->request(), $request_params);
        }

        foreach ($required_fields as $field) {
            if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0)
                $error = true;
        }

        if ($error) return false;

        return true;
    }

    public function apiMsg($code, $error = null, $data = null)
    {
        require_once('Message.php');

        $return = array(
            'code' => $arrMessage[$code]['code'],
            'status' => $arrMessage[$code]['status'],
            'error' => $error
        );

        if(! is_array($data)) {
            $data = (array) $data;
        }

        $return = array_merge($return, $data);

        return $return;
    }

}
