<?php
$arrMessage = array();

$arrMessage = array(
    '100' => array('code' => '100', 'status' => 'SUCCESS'),
    '110' => array('code' => '110', 'status' => 'PARAMETER_NOT_FOUND'),
    '120' => array('code' => '120', 'status' => 'DATA_NOT_FOUND'),
    '400' => array('code' => '400', 'status' => 'NOT_AUTH_LOGIN'),
    '900' => array('code' => '900', 'status' => 'ERROR')
);
