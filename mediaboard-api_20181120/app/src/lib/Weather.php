<?php

namespace lib;

use config\Config as Config;
use db\MySQLDB as DB;

class Weather {
	private static $self;

	private $db;
	private $common;

	private $apiKey;

	public function __construct() {
		$this->db     = DB::getInstance();
		$this->common = Common::getInstance();

		$this->apiKey = Config::get( 'api.weather.key' );
	}

	public static function getInstance() {
		if ( ! self::$self ) {
			self::$self = new Weather();
		}

		return self::$self;
	}

	// 기상 API 정보
	private static $weatherTable = array(
		"avail"    => array(
			"0040",
			"0140",
			"0240",
			"0340",
			"0440",
			"0540",
			"0640",
			"0740",
			"0840",
			"0940",
			"1040",
			"1140",
			"1240",
			"1340",
			"1440",
			"1540",
			"1640",
			"1740",
			"1840",
			"1940",
			"2040",
			"2140",
			"2240",
			"2340"
		),
		"basetime" => array(
			"0000",
			"0100",
			"0200",
			"0300",
			"0400",
			"0500",
			"0600",
			"0700",
			"0800",
			"0900",
			"1000",
			"1100",
			"1200",
			"1300",
			"1400",
			"1500",
			"1600",
			"1700",
			"1800",
			"1900",
			"2000",
			"2100",
			"2200",
			"2300"
		)
	);

	private static $lowTempTable = array(
		"avail"    => array( "0210" ),
		"basetime" => array( "0200" )
	);

	private static $highTempTable = array(
		"avail"    => array( "0210", "0510", "0810", "1110" ),
		"basetime" => array( "0200", "0500", "0800", "1100" )
	);

	// 생활지수 API 정보
	private static $uvRayTable = array(
		"month"    => array( "03", "04", "05", "06", "07", "08", "09", "10", "11" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	private static $windChillTable = array(
		"month"    => array( "11", "12", "01", "02", "03" ),
		"avail"    => array( "01", "04", "07", "10", "13", "16", "19", "22" ),
		"basetime" => array( "00", "03", "06", "09", "12", "15", "18", "21" )
	);

	private static $foodPoisoningTable = array(
		"month"    => array( "03", "04", "05", "06", "07", "08", "09", "10", "11" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	private static $frozenTable = array(
		"month"    => array( "12", "01", "02" ),
		"avail"    => array( "01", "04", "07", "10", "13", "16", "19", "22" ),
		"basetime" => array( "00", "03", "06", "09", "12", "15", "18", "21" )
	);

	private static $discomfortTable = array(
		"month"    => array( "06", "07", "08", "09" ),
		"avail"    => array( "01", "04", "07", "10", "13", "16", "19", "22" ),
		"basetime" => array( "00", "03", "06", "09", "12", "15", "18", "21" )
	);

	// 보건지수 API 정보
	public static $coldTable = array(
		"month"    => array( "09", "10", "11", "12", "01", "02", "03", "04" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	private static $skinDiseaseTable = array(
		"month"    => array( "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	private static $respiratoryDiseaseTable = array(
		"month"    => array( "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	private static $strokeTable = array(
		"month"    => array( "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ),
		"avail"    => array( "06" ),
		"basetime" => array( "06" )
	);

	// 각 테이블로부터 파라미터용 시간을 가져옴
	private function getBaseTime( $table ) {
		$now      = date( "Hi" );
		$avail    = $table["avail"];
		$basetime = $table["basetime"];

		$i = 0;
		while ( $i < count( $avail ) ) {
			if ( strcmp( $now, $avail[ $i ] ) < 0 ) {
				break;
			}
			$i += 1;
		}

		if ( $i == 0 ) {
			return null;
		}

		return $basetime[ $i - 1 ];
	}

	// 호출 실패시 이전 실황 획득을 위한 recursive
	private function callWeatherApi( $url, $serviceKey, $nx, $ny, $base_date, $base_time ) {
		$result = array();

		$params = "?ServiceKey=" . $serviceKey . "&base_date=" . $base_date . "&base_time=" . $base_time . "&nx=" . $nx . "&ny=" . $ny . "&numOfRows=99&_type=json";

		$response = $this->getRequest( $url . $params );

		$data = json_decode( $response, true );
		//$this->common->log(__METHOD__ . 'data:', $data);

		if ( ! is_null( $data ) ) {
			$item = $data['response']['body']['items']['item'];

			$result['SKY'] = $this->findValueByKey( $item, 'obsrValue', 'category', 'SKY' );
			$result['PTY'] = $this->findValueByKey( $item, 'obsrValue', 'category', 'PTY' );
			$result['T1H'] = $this->findValueByKey( $item, 'obsrValue', 'category', 'T1H' );
		} else {
			$idx = array_search( $base_date, $this::$weatherTable["basetime"] );

			if ( $idx > 0 ) {
				$base_date = $this::$weatherTable["basetime"][ $idx - 1 ];
				$result    = $this->callWeatherApi( $serviceKey, $nx, $ny, $base_date, $base_time );
			} else {
				$result = array();
			}
		}

		//$this->common->log(__METHOD__. ' result: ', $result)

		return $result;
	}

	// API 주소로 GET 리퀘스트를 보냄
	private function getRequest( $url ) {
		$ch = curl_init();

		// Set query data here with the URL
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 3 );
		$response = trim( curl_exec( $ch ) );
		curl_close( $ch );

		return $response;
	}

	// 목록으로부터 키에 맞는 값을 찾아서 반환
	private function findValueByKey( $list, $valueTag, $keyTag, $key, $keyTag2 = '', $key2 = '' ) {
		$value = "";

		for ( $i = 0; $i < count( $list ); $i += 1 ) {

			if ( $list[ $i ][ $keyTag ] == $key ) {
				if ( $keyTag2 == '' || $list[ $i ][ $keyTag2 ] == $key2 ) {
					$value = $list[ $i ][ $valueTag ];
					break;
				}
			}
		}

		return $value;
	}

	// 각 테이블로부터 파라미터용 시각을 가져옴
	function getBaseHour( $table ) {
		$now      = date( "H" );
		$avail    = $table["avail"];
		$basetime = $table["basetime"];

		$i = 0;
		while ( $i < count( $avail ) ) {
			if ( strcmp( $now, $avail[ $i ] ) < 0 ) {
				break;
			}
			$i += 1;
		}

		if ( $i == 0 ) {
			return null;
		}

		return $basetime[ $i - 1 ];
	}

	// 각 테이블로부터 사용 가능한 기간인지 확인
	function checkAvailableMonth( $table ) {
		$month = date( "m" );

		return in_array( $month, $table["month"] );
	}

	// 초단기 실황조회 - 현재 날씨, 현재 온도
	public function getWeather( $nx, $ny ) {
		$url = "http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastGrib";

		$base_date = date( "Ymd" );
		$base_time = $this->getBaseTime( $this::$weatherTable );

		$result = array();

		if ( is_null( $base_time ) ) {
			return $result;
		}

		$result = $this->callWeatherApi( $url, $this->apiKey, $nx, $ny, $base_date, $base_time );

		return $result;
	}

	// 동네예보조회 - 최저온도
	public function getLowTemperature( $nx, $ny ) {
		$url = "http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastSpaceData";

		$base_date = date( "Ymd" );
		$base_time = $this->getBaseTime( $this::$lowTempTable );

		//$result = array();
		$result = new \stdClass();

		if ( is_null( $base_time ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&base_date=" . $base_date . "&base_time=" . $base_time . "&nx=" . $nx . "&ny=" . $ny . "&numOfRows=999&_type=json";

		$response = $this->getRequest( $url . $params );

		$data = json_decode( $response, true );

		if ( ! is_null( $data ) ) {
			$item = $data['response']['body']['items']['item'];
			//$result['TMN'] = $this->findValueByKey($item, 'fcstValue', 'fcstDate', $base_date, 'category', 'TMN');
			//$this->common->log(gettype($this->findValueByKey($item, 'fcstValue', 'fcstDate', $base_date, 'category', 'TMN')));
			$result->TMN = $this->findValueByKey( $item, 'fcstValue', 'fcstDate', $base_date, 'category', 'TMN' );
		}

		return $result;
	}

	// 동네예보조회 - 최고온도
	public function getHighTemperature( $nx, $ny ) {
		$url = "http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastSpaceData";

		$base_date = date( "Ymd" );
		$base_time = $this->getBaseTime( $this::$highTempTable );

		//$result = array();
		$result = new \stdClass();

		if ( is_null( $base_time ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&base_date=" . $base_date . "&base_time=" . $base_time . "&nx=" . $nx . "&ny=" . $ny . "&numOfRows=999&_type=json";

		$response = $this->getRequest( $url . $params );

		$data = json_decode( $response, true );

		if ( ! is_null( $data ) ) {
			$item = $data['response']['body']['items']['item'];
			//$result['TMX'] = $this->findValueByKey($item, 'fcstValue', 'fcstDate', $base_date, 'category', 'TMX');
			$result->TMX = $this->findValueByKey( $item, 'fcstValue', 'fcstDate', $base_date, 'category', 'TMX' );
		}

		return $result;
	}

	// 자외선지수 조회
	public function getUvRay( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveLifeIndexService2/getUltrvLifeList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$uvRayTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$uvRayTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			$result->today = $data['Body']['IndexModel']['today'];
			//$result->today = (float) $data['Body']['IndexModel']['today'];
		}

		return $result;
	}

	// 체감온도 조회
	public function getWindChill( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveLifeIndexService2/getSensorytemLifeList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$windChillTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$windChillTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );

		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['h3'] = $data['Body']['IndexModel']['h3'];
			$result->h3 = $data['Body']['IndexModel']['h3'];
		}

		return $result;
	}

	// 식중독지수 조회
	public function getFoodPoisoning( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveLifeIndexService2/getFsnLifeList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$foodPoisoningTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$foodPoisoningTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			$result->today = $data['Body']['IndexModel']['today'];
		}

		return $result;
	}

	// 동파가능지수 조회
	public function getFrozen( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveLifeIndexService2/getWinterLifeList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$frozenTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$frozenTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['h3'] = $data['Body']['IndexModel']['h3'];
			$result->h3 = $data['Body']['IndexModel']['h3'];
		}

		return $result;
	}

	// 불쾌지수 조회
	public function getDiscomfort( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveLifeIndexService2/getDsplsLifeList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$discomfortTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$discomfortTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['h3'] = $data['Body']['IndexModel']['h3'];
			$result->h3 = $data['Body']['IndexModel']['h3'];
		}

		return $result;
	}

	// 감기지수 조회
	public function getCold( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveWhoIndexService2/getInflWhoList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$coldTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$coldTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			$result->today = $data['Body']['IndexModel']['today'];
		}

		return $result;
	}

	// 피부질환지수 조회
	public function getSkinDisease( $areaNo ) {
		//$this->common->log("gettype=" . gettype($areaNo) . "|areaNo=", $areaNo);
		$url = "http://newsky2.kma.go.kr/iros/RetrieveWhoIndexService2/getSkinWhoList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$skinDiseaseTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$skinDiseaseTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );
		//$this->common->log("gettype=" . gettype($data) . "|data=", $data);

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			$result->today = $data['Body']['IndexModel']['today'];
			//$this->common->log("|" . $result->today . "|gettype=" . gettype($result->today));
		}

		return $result;
	}

	// 호흡기질환지수 조회
	public function getRespiratoryDisease( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveWhoIndexService2/getAsthmaWhoList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$respiratoryDiseaseTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$respiratoryDiseaseTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			$result->today = $data['Body']['IndexModel']['today'];
		}

		return $result;
	}

	// 뇌졸중가능지수 조회
	public function getStroke( $areaNo ) {
		$url = "http://newsky2.kma.go.kr/iros/RetrieveWhoIndexService2/getBrainWhoList";

		$base_date = date( "Ymd" );
		$base_hour = $this->getBaseHour( $this::$strokeTable );

		//$result = array();
		$result = new \stdClass();

		if ( ( ! $this->checkAvailableMonth( $this::$strokeTable ) ) || is_null( $base_hour ) ) {
			return $result;
		}

		$params = "?ServiceKey=" . $this->apiKey . "&time=" . $base_date . $base_hour . "&areaNo=" . $areaNo;

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			//$result['today'] = $data['Body']['IndexModel']['today'];
			//$this->common->log(gettype($data['Body']['IndexModel']['today']));
			$result->today = $data['Body']['IndexModel']['today'];
		}

		return $result;
	}

	// 미세먼지 조회
	public function getParticulate( $sido, $city ) {
		$url = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnMesureSidoLIst";

		//$result = array();
		$result = new \stdClass();

		$params = "?ServiceKey=" . $this->apiKey . "&sidoName=" . urlencode( $sido );
		$params .= "&searchCondition=HOUR&pageNo=1&numOfRows=999";

		$response = $this->getRequest( $url . $params );

		$xml  = simplexml_load_string( $response );
		$json = json_encode( $xml );
		$data = json_decode( $json, true );

		if ( ! is_null( $data ) ) {
			$item = $data['body']['items']['item'];
			$city = $this->checkMetropolitanCity($sido, $city);
			//$result['pm10Value'] = $this->findValueByKey($item, 'pm10Value', 'cityName', $city);
			$result->pm10Value = $this->findValueByKey( $item, 'pm10Value', 'cityName', $city );
		}

		return $result;
	}

	// 미세먼지 조회 시, 특별시/광역시가 아닌 경우를 체크
	private function checkMetropolitanCity($sido, $city) {
	    $metropolitanCities = array("서울", "부산", "대구", "대전", "광주", "인천", "울산");

	    if (! in_array($sido, $metropolitanCities)) {
	        $temp = explode(" ", $city);
	        return is_array($temp) ? $temp[0] : $city;
	    }

            return $city;
    	}

}
