<?php
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('APP_ROOT', ROOT . '/app/');
define('SRC_ROOT', APP_ROOT . '/src/');

require_once APP_ROOT . 'autoload.php';
require_once ROOT . '/vendor/autoload.php';

use config\Config as Config;
use lib\Common as Common;

$config = new Config();
$common = new Common();
